import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  pictureContainer: {
    position: 'relative',
    transition: 'transform 0.3s ease-in-out',
    '&:hover': { transform: 'scale3d(1.08, 1.08, 1)' },
  },
  picture: {
    width: theme.spacing(30),
    height: theme.spacing(30),
    filter: 'brightness(60%)',
  },
  overlay: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    color: '#FAFAFA',
    transform: 'translate(-50%, -50%)',
    fontWeight: 'bold',
  },
}));

const GenreSimple = ({ name, picture }) => {
  const classes = useStyles();
  return (
    <Grid
      item
      className={classes.pictureContainer}
    >
      <Avatar
        className={classes.picture}
        src={picture}
        alt={name}
      />
      <Typography
        variant="h5"
        className={classes.overlay}
        align="center"
      >
        {name}
      </Typography>
    </Grid>
  );
};

GenreSimple.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
};

export default GenreSimple;
