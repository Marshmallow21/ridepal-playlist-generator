import {
  Grid
} from '@material-ui/core';

const ServerError = () => (
  <Grid
    item
    container
    justify="center"
  >
    <img src="/images/error-500.jpg" alt="ERROR 500 - Internal server error" />
  </Grid>
);

export default ServerError;
