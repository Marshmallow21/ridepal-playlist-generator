import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';

import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  picture: {
    width: theme.spacing(25),
    height: theme.spacing(25),
    transition: 'transform 0.20s ease-in-out',
    '&:hover': { transform: 'scale3d(1.02, 1.02, 1)' },

  },
}));

const ArtistSimple = ({ name, picture }) => {
  const classes = useStyles();

  return (
    <Grid item>
      <Tooltip
        className={classes.tooltip}
        title={name}
        arrow
      >
        <Avatar
          variant="rounded"
          className={classes.picture}
          src={picture}
          alt={name}
        />
      </Tooltip>
    </Grid>
  );
};

ArtistSimple.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
};

export default ArtistSimple;
