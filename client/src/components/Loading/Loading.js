import {
  Grid, CircularProgress
} from '@material-ui/core';

const Loading = () => (
  <Grid
    item
    container
    justify="center"
  >
    <CircularProgress disableShrink color='secondary' />
  </Grid>
);

export default Loading;
