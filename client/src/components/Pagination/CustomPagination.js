import { Pagination } from '@material-ui/lab';
import PropTypes from 'prop-types';

const CustomPagination = ({
  numberOfPages, page, setPage, setIsSearchTriggered
}) => {
  const handlePagination = (_, pageNumber) => {
    if (setIsSearchTriggered) setIsSearchTriggered(true);
    setPage(pageNumber);
  };

  return (
    <Pagination
      page={page}
      onChange={handlePagination}
      count={Number(numberOfPages)}
      showFirstButton
      showLastButton
    />
  );
};

CustomPagination.propTypes = {
  numberOfPages: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  setIsSearchTriggered: PropTypes.func,
};

CustomPagination.defaultProps = {
  setIsSearchTriggered: null,
};

export default CustomPagination;
