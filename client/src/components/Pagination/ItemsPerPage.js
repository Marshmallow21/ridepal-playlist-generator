import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: theme.spacing(9),
  },
  select: {
    paddingTop: 10,
    paddingBottom: 10,
  }
}));

const ItemsPerPage = ({ itemsPerPage, setItemsPerPage }) => {
  const classes = useStyles();

  const handleChange = (event) => {
    setItemsPerPage(event.target.value);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel id="items-label">Show by</InputLabel>
      <Select
        labelId="items-label"
        id="items-select"
        value={itemsPerPage}
        onChange={handleChange}
        label='Show by'
        color='primary'
        classes={{ root: classes.select }}
      >
        <MenuItem value={12}>12</MenuItem>
        <MenuItem value={20}>20</MenuItem>
        <MenuItem value={32}>32</MenuItem>
      </Select>
    </FormControl>
  );
};

ItemsPerPage.propTypes = {
  itemsPerPage: PropTypes.number.isRequired,
  setItemsPerPage: PropTypes.func.isRequired,
};

export default ItemsPerPage;
