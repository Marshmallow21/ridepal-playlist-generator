import { useContext } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import PropTypes from 'prop-types';

import authService from 'services/auth';
import { AuthContext } from 'providers/АuthContext';

const useStyles = makeStyles((theme) => ({
  navButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },

}));

const UserProfileMenu = ({
  userMenuAnchorEl, handleMenuClose, isUserMenuOpen
}) => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useContext(AuthContext);

  const logoutHandler = () => {
    handleMenuClose();
    authService.logout()
      .then(() => {
        auth.setUser(null);
        auth.setAvatar(null);
        localStorage.removeItem('token');
        history.push('/home');
      })
      .catch(() => history.push('/500'));
  };

  return (
    <>
      <Menu
        anchorEl={userMenuAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'left' }}
        open={isUserMenuOpen}
        onClose={handleMenuClose}
      >
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to={{ pathname: `/profile/${auth.user.username}`, state: auth.user.id }}
          onClick={handleMenuClose}
        >
          Profile
        </MenuItem>
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to='/home'
          onClick={logoutHandler}
        >
          Logout
        </MenuItem>
      </Menu>
    </>

  );
};

UserProfileMenu.propTypes = {
  userMenuAnchorEl: PropTypes.instanceOf(Element),
  isUserMenuOpen: PropTypes.bool.isRequired,
  handleMenuClose: PropTypes.func.isRequired,
};

UserProfileMenu.defaultProps = {
  userMenuAnchorEl: null,
};

export default UserProfileMenu;
