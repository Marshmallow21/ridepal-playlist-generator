import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  navButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },
}));

const AdminMenu = ({
  adminMenuAnchorEl, handleMenuClose, isAdminMenuOpen
}) => {
  const classes = useStyles();

  return (
    <>
      <Menu
        anchorEl={adminMenuAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'left' }}
        open={isAdminMenuOpen}
        onClose={handleMenuClose}
      >
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to="/admin/playlists"
          onClick={handleMenuClose}
        >
          Playlists
        </MenuItem>
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to="/admin/users"
          onClick={handleMenuClose}
        >
          Users
        </MenuItem>
      </Menu>
    </>

  );
};

AdminMenu.propTypes = {
  adminMenuAnchorEl: PropTypes.instanceOf(Element),
  isAdminMenuOpen: PropTypes.bool.isRequired,
  handleMenuClose: PropTypes.func.isRequired,
};

AdminMenu.defaultProps = {
  adminMenuAnchorEl: null,
};

export default AdminMenu;
