import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  navButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },
}));

const AuthMenu = ({
  authMenuAnchorEl, isAuthMenuOpen, handleMenuClose
}) => {
  const classes = useStyles();

  return (
    <Menu
      anchorEl={authMenuAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isAuthMenuOpen}
      onClose={handleMenuClose}
    >
      <>
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to="/login"
          onClick={handleMenuClose}
        >
          Login
        </MenuItem>
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to="/register"
          onClick={handleMenuClose}
        >
          Register
        </MenuItem>
      </>
    </Menu>
  );
};

AuthMenu.propTypes = {
  authMenuAnchorEl: PropTypes.instanceOf(Element),
  isAuthMenuOpen: PropTypes.bool.isRequired,
  handleMenuClose: PropTypes.func.isRequired,
};

AuthMenu.defaultProps = {
  authMenuAnchorEl: null,
};

export default AuthMenu;
