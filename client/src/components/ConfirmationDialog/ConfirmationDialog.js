import {
  Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle
} from '@material-ui/core';
import PropTypes from 'prop-types';

const ConfirmationDialog = ({ actionConfirm, handleClose }) => (
  <Dialog
    open={actionConfirm.isOpen}
    onClose={handleClose}
  >
    <DialogTitle>{`${actionConfirm.title}`}</DialogTitle>
    <DialogContent>
      <DialogContentText>
        {`${actionConfirm.text}`}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button
        onClick={() => {
          actionConfirm.action();
          handleClose();
        }}
        color="secondary"
      >
        Confirm
      </Button>
      <Button onClick={handleClose} color="primary">
        Cancel
      </Button>
    </DialogActions>
  </Dialog>
);

ConfirmationDialog.propTypes = {
  handleClose: PropTypes.func.isRequired,
  actionConfirm: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
  }).isRequired,
};

export default ConfirmationDialog;
