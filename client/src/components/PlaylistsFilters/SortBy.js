import { useContext, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ArrowUpwardRoundedIcon from '@material-ui/icons/ArrowUpwardRounded';
import ArrowDownwardRoundedIcon from '@material-ui/icons/ArrowDownwardRounded';

import { FiltersContext } from 'providers/FiltersContext';
import { sortByItems, sortDirection } from 'common/filters.js';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
  },
  sortControls: {
    marginTop: theme.spacing(1),
  },
}));

const SortBy = () => {
  const classes = useStyles();

  const { sortBy, setSortBy } = useContext(FiltersContext);
  const [initialValue, initialDirection] = sortBy.split('.');
  const [sortByDirection, setSortByDirection] = useState(`.${initialDirection}`);
  const [sortByValue, setSortByValue] = useState(initialValue);

  useEffect(() => {
    setSortBy(`${sortByValue}${sortByDirection}`);
  }, [sortByValue, sortByDirection]);

  const handleSortValue = (event) => {
    setSortByValue(event.target.value);
  };

  const handleSortDirection = () => {
    setSortByDirection(sortByDirection === sortDirection.ASC
      ? sortDirection.DESC
      : sortDirection.ASC);
  };

  return (
    <FormControl
      component="fieldset"
      className={classes.formControl}
    >
      <Typography variant='subtitle1'>
        Sort by
      </Typography>
      <Divider />
      <Grid
        container
        justify='space-between'
        alignItems="center"
        className={classes.sortControls}
      >
        <Grid item>
          <Select
            variant="outlined"
            value={sortByValue}
            onChange={handleSortValue}
          >
            {Object.values(sortByItems).map(item => (
              <MenuItem
                key={item}
                value={item}
              >
                {item.charAt(0).toUpperCase() + item.slice(1)}
              </MenuItem>
            ))}
          </Select>
        </Grid>
        <Grid item>
          <Tooltip title="Sort direction">
            <IconButton
              onClick={handleSortDirection}
            >
              {sortByDirection === sortDirection.ASC && <ArrowUpwardRoundedIcon />}
              {sortByDirection === sortDirection.DESC && <ArrowDownwardRoundedIcon />}
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    </FormControl>
  );
};

export default SortBy;
