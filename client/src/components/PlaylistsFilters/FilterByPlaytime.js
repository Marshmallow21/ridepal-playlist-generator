import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import { FiltersContext } from 'providers/FiltersContext';
import { playtimeWindows } from 'common/filters.js';
import { Duration } from 'luxon';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
  },
}));

const FilterByPlaytime = () => {
  const classes = useStyles();
  const { playtimeFilter, setPlaytimeFilter } = useContext(FiltersContext);

  const handleChange = (event) => {
    setPlaytimeFilter({ ...playtimeFilter, [event.target.name]: event.target.checked });
  };

  return (
    <FormControl
      component="fieldset"
      className={classes.formControl}
    >
      <Typography variant='subtitle1'>
        Playtime
      </Typography>
      <Divider />
      <FormGroup>
        {playtimeWindows.map((window, index) => (
          <FormControlLabel
            key={window.min}
            control={(
              <Checkbox
                checked={playtimeFilter[`${window.min}-${window.max}`]}
                onChange={handleChange}
                name={`${window.min}-${window.max}`}
              />
            )}
            label={
              index === 0
                ? `Up to ${Duration.fromMillis(window.max * 1000).toFormat('m')} minutes`
                : `${(Duration.fromMillis(window.min * 1000) < 60 * 60 * 1000)
                  ? (`${Duration.fromMillis(window.min * 1000).toFormat('m')} minutes`)
                  : (`${Duration.fromMillis(window.min * 1000).toFormat('h')}`)
                }
                - ${+(Duration.fromMillis(window.max * 1000).toFormat('h')) === 1
                  ? (`${Duration.fromMillis(window.max * 1000).toFormat('h')} hour`)
                  : (`${Duration.fromMillis(window.max * 1000).toFormat('h')} hours`)}`
            }
          />
        ))}
      </FormGroup>
    </FormControl>
  );
};

export default FilterByPlaytime;
