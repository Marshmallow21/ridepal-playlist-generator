/* eslint-disable no-nested-ternary */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import adminService from 'services/admin';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import ActionAlert from 'components/ActionAlert/ActionAlert';
import FilterButton from 'components/Admin/FilterButton';
import ConfirmationDialog from 'components/ConfirmationDialog/ConfirmationDialog';
import Loading from 'components/Loading/Loading';
import TuneIcon from '@material-ui/icons/Tune';
import Grid from '@material-ui/core/Grid';

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '1 1 100%',
    fontWeight: 'bold',
  },
}));

const AdminTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const {
    numSelected, selected, setSelected, setRows, rows,
    optionsRole, setFilter,
    filter, optionsPlaylist, setFiltersBarOpen, label
  } = props;

  const history = useHistory();
  const [filterBar, setFilterBar] = useState(false);
  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });

  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });
  const [actionConfirm, setActionConfirm] = useState({
    isOpen: false,
    title: '',
    text: '',
    action: () => { },
  });

  const handleCloseDialog = () => setActionConfirm(
    {
      isOpen: false, title: '', text: '', action: () => { },
    }
  );

  const TogleFiltersBar = () => {
    setFilterBar(!filterBar);
    setFilter({});
  };

  const openFiltersBar = () => {
    setFiltersBarOpen(true);
  };

  const deleteUsers = (id) => {
    setLoading(true);
    adminService.deleteUser(+id)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else {
          setRows(rows.filter(user => user.id !== id));
          setSelected();
          setNotification({
            isOpen: true,
            type: 'success',
            message: `Successfully deleted user with id ${id}!`,
          });
        }
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
    setSelected([]);
  };

  const handleDelete = () => {
    setActionConfirm(
      {
        isOpen: true,
        title: 'Confirm deletion',
        text: `Would you like to proceed with id ${selected} deletion?`,
        action: () => deleteUsers(selected),
      }
    );
  };

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected}
          {' '}
          selected
        </Typography>
      ) : (
        <Typography
          className={classes.title}
          variant="h6"
          id="tableTitle"
          color='secondary'
        >
          {label}
        </Typography>
      )}
      {filterBar && (
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={10}
        >
          <Grid item>
            <FilterButton
              lables={optionsRole}
              setFilter={setFilter}
              filter={filter}
              searchedField="role"
            />
          </Grid>
          <Grid item>
            <FilterButton
              lables={optionsPlaylist}
              setFilter={setFilter}
              filter={filter}
              searchedField="playlistsNumber"
            />
          </Grid>
        </Grid>
      )}
      { numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete" onClick={handleDelete} color='secondary'>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        (label === 'Playlists') ? (
          <>
            <Grid item>
              <IconButton
                onClick={openFiltersBar}
              >
                <TuneIcon
                  color='secondary'
                />
              </IconButton>
            </Grid>
          </>
        )
          : (
            <Tooltip title="Filter list">
              <IconButton aria-label="filter list" onClick={TogleFiltersBar}>
                <FilterListIcon color='secondary' />
              </IconButton>
            </Tooltip>
          )
      )}
      {loading && <Loading />}

      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
      <ConfirmationDialog
        actionConfirm={actionConfirm}
        handleClose={handleCloseDialog}
      />
    </Toolbar>
  );
};

AdminTableToolbar.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.shape({
    genre: PropTypes.string,
    user_id: PropTypes.number,
    id: PropTypes.number,
    genres: PropTypes.string,
    title: PropTypes.string,
    playtime: PropTypes.number,
    rank: PropTypes.number,
    created_on: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired).isRequired,
  numSelected: PropTypes.number,
  setSelected: PropTypes.func,
  setRows: PropTypes.func.isRequired,
  optionsPlaylist: PropTypes.arrayOf(PropTypes.string),
  optionsRole: PropTypes.arrayOf(PropTypes.string),
  filter: PropTypes.objectOf(PropTypes.string),
  selected: PropTypes.arrayOf(PropTypes.number),
  setFilter: PropTypes.func,
  label: PropTypes.string.isRequired,
  setFiltersBarOpen: PropTypes.func,
};

AdminTableToolbar.defaultProps = {
  setFiltersBarOpen: undefined,
  setSelected: undefined,
  numSelected: undefined,
  optionsPlaylist: undefined,
  optionsRole: undefined,
  selected: undefined,
  setFilter: undefined,
  filter: undefined
};

export default AdminTableToolbar;
