import { PieChart } from 'react-minimal-pie-chart';

import { Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import PropTypes from 'prop-types';

const PieChartGenres = ({
  data, labelChart, handleFilter
}) => (

  <>
    <Typography
      gutterBottom
      align="center"
      variant="inherit"
      component="h4"
    >
      {labelChart}
    </Typography>
    {!data[0]?.value
      ? (<Skeleton variant="circle" width={300} height={300} />)
      : (
        <PieChart
          lineWidth={65}
          data={data}
          paddingAngle={0}
          label={({ dataEntry }) => `${Math.round(dataEntry.percentage)}% ${dataEntry.title}`}
          labelStyle={{
            fontSize: '4px',
            fontFamily: 'sans-serif',
          }}
          labelPosition={67}
          onClick={(e, segmentIndex) => handleFilter({ genreId: data[segmentIndex].genreId })}
        />
      )}
  </>

);
PieChartGenres.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    genreId: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
  })).isRequired,
  handleFilter: PropTypes.func.isRequired,
  labelChart: PropTypes.string.isRequired,
};
export default PieChartGenres;
