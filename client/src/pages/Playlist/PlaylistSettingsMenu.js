import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import ActionAlert from 'components/ActionAlert/ActionAlert';
import ConfirmationDialog from 'components/ConfirmationDialog/ConfirmationDialog';
import playlistService from 'services/playlists.js';

const PlaylistSettingsMenu = ({
  isOpen,
  handleClose,
  anchorEl,
  handleEditFormOpen,
  id
}) => {
  const history = useHistory();
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const [actionConfirm, setActionConfirm] = useState({
    isOpen: false,
    title: '',
    text: '',
    action: () => { },
  });

  const deletePlaylist = () => {
    playlistService.deletePlaylist(id)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else {
          setNotification({
            isOpen: true,
            type: 'success',
            message: `Successfully deleted playlist with id ${id}!`,
          });
          history.push('/playlists');
        }
      })
      .catch(() => history.push('/500'));
  };

  const handleDeleteBtn = () => {
    handleClose();
    setActionConfirm(
      {
        isOpen: true,
        title: 'Confirm deletion',
        text: `Would you like to proceed with id ${id} deletion?`,
        action: () => deletePlaylist(id),
      }
    );
  };

  const handleEditBtn = () => {
    handleClose();
    handleEditFormOpen();
  };

  const handleCloseDialog = () => setActionConfirm(
    {
      isOpen: false, title: '', text: '', action: () => { },
    }
  );

  return (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isOpen}
      onClose={handleClose}
    >
      <Grid>
        <MenuItem
          onClick={handleEditBtn}
        >
          Edit playlist
        </MenuItem>
        <MenuItem
          onClick={handleDeleteBtn}
        >
          Delete playlist
        </MenuItem>
      </Grid>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
      <ConfirmationDialog
        actionConfirm={actionConfirm}
        handleClose={handleCloseDialog}
      />
    </Menu>
  );
};

PlaylistSettingsMenu.propTypes = {
  anchorEl: PropTypes.instanceOf(Element),
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleEditFormOpen: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
};

PlaylistSettingsMenu.defaultProps = {
  anchorEl: null,
};

export default PlaylistSettingsMenu;
