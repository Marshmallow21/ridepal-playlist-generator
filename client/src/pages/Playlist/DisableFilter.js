import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import utils from 'common/utils';

const useStyles = makeStyles((theme) => ({
  disableFilterButton: {
    backgroundColor: utils.pickColor(),
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.secondary.light
    }
  },
}));

const DisableFilter = ({ handleDisableFilter }) => {
  const classes = useStyles();
  return (
    <Button
      className={classes.disableFilterButton}
      fullWidth
      onClick={handleDisableFilter}
    >
      Disable filter
    </Button>
  );
};

DisableFilter.propTypes = {
  handleDisableFilter: PropTypes.func.isRequired,
};
export default DisableFilter;
