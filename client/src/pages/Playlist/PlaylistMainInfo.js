import { useContext, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import { Duration, DateTime } from 'luxon';
import PropTypes from 'prop-types';

import { AuthContext } from 'providers/АuthContext';
import PlaylistSettingsMenu from 'pages/Playlist/PlaylistSettingsMenu';
import EditPlaylist from 'components/EditPlaylist/EditPlaylist';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
  pictureContainer: {
    '&:hover > $overlay': {
      display: 'block',
    },
    '&:hover > $picture': {
      filter: 'brightness(60%)',
    },
    position: 'relative',
  },
  picture: {
    width: theme.spacing(25),
    height: theme.spacing(25),
  },
  overlay: {
    color: '#FAFAFA',
    display: 'none',
    position: 'absolute',
    top: '50%',
    left: '50%',
    fontSize: theme.spacing(6),
    transform: 'translate(-50%, -50%)',
  },
  bold: {
    fontWeight: 'bold',
  }
}));

const PlaylistMainInfo = ({
  id,
  username,
  userId,
  title,
  picture,
  rank,
  numberOfTracks,
  createdOn,
  setPlaylist,
  duration,
}) => {
  const classes = useStyles();
  const auth = useContext(AuthContext);

  const [settingsMenuAnchorEl, setSettingsMenuAnchorEl] = useState(null);
  const isSettingsMenuOpen = Boolean(settingsMenuAnchorEl);
  const [isEditFormOpen, setEditFormOpen] = useState(false);
  const handleEditFormClose = () => setEditFormOpen(false);
  const handleEditFormOpen = () => setEditFormOpen(true);

  const handleSettingsMenuClose = () => {
    setSettingsMenuAnchorEl(null);
  };

  const handleSettingsMenuOpen = (event) => {
    event.preventDefault();
    setSettingsMenuAnchorEl(event.currentTarget);
  };

  return (
    <Grid
      item
      container
      alignItems="flex-start"
      justify="space-between"
    >
      <Grid item>
        <Grid
          container
          alignItems="flex-end"
          spacing={4}
        >
          <Grid
            item
            className={userId === auth.user?.id
              ? classes.pictureContainer
              : ''}
          >
            {!picture
              ? (<Skeleton variant="rect" height={200} width={200} />)
              : (
                <Avatar
                  variant="rounded"
                  className={classes.picture}
                  alt={title}
                  src={picture && picture.includes('unsplash')
                    ? picture
                    : `/images/${picture}`}
                />
              )}
            {userId === auth.user?.id && (
              <AddPhotoAlternateIcon
                className={classes.overlay}
                onClick={handleEditFormOpen}
              />
            )}
          </Grid>
          <Grid item>
            {!title ? (<Skeleton height={30} width={100} />) : (
              <Typography
                variant="h5"
                color="secondary"
                className={classes.bold}
                gutterBottom
              >
                {title}
              </Typography>
            )}

            <Grid
              container
              spacing={2}
              alignItems="flex-end"
            >
              <Grid item>
                <Grid
                  container
                  alignItems="center"
                  spacing={1}
                >
                  <Grid item>
                    <MusicNoteIcon
                      color="secondary"
                    />
                  </Grid>
                  <Grid item>

                    {!numberOfTracks ? (<Skeleton height={20} width={85} />) : (
                      <Typography
                        variant="subtitle1"
                        color="textSecondary"
                      >
                        {`${numberOfTracks} songs`}
                      </Typography>
                    )}

                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  alignItems="center"
                  spacing={1}
                >
                  <Grid item>
                    <AccessTimeIcon
                      color="secondary"
                    />
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      color="textSecondary"
                    >
                      {!duration ? (<Skeleton height={20} width={85} />)
                        : (Duration.fromMillis(duration * 1000).toFormat('hh:mm:ss'))}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  alignItems="center"
                  spacing={1}
                >
                  <Grid item>
                    <EqualizerIcon
                      color="secondary"
                    />
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      color="textSecondary"
                    >
                      {!rank ? (<Skeleton height={20} width={85} />) : (rank)}

                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Typography
              variant="subtitle1"
              color="textSecondary"
            >
              {!createdOn ? (<Skeleton height={20} width={160} />) : (`Created on: ${DateTime.fromISO(createdOn).toFormat('dd LLLL yyyy')}`)}
            </Typography>
            <Typography
              variant="subtitle1"
            >
              <Link
                color="primary"
                component={NavLink}
                to={{ pathname: `/profile/${username}`, state: userId }}
              >
                {!username ? (<Skeleton height={20} width={145} />) : (`by ${username}`)}

              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        {userId === auth.user?.id && (
          <>
            <IconButton
              color="primary"
              onClick={handleSettingsMenuOpen}
            >
              <MoreHorizIcon fontSize="large" />
            </IconButton>
            <PlaylistSettingsMenu
              handleClose={handleSettingsMenuClose}
              anchorEl={settingsMenuAnchorEl}
              isOpen={isSettingsMenuOpen}
              handleEditFormOpen={handleEditFormOpen}
              id={id}
            />
          </>
        )}
        <EditPlaylist
          isOpen={isEditFormOpen}
          handleClose={handleEditFormClose}
          playlist={{ id, title, picture }}
          setPlaylist={setPlaylist}
        />
      </Grid>
    </Grid>
  );
};

PlaylistMainInfo.propTypes = {
  id: PropTypes.number,
  userId: PropTypes.number,
  username: PropTypes.string,
  title: PropTypes.string,
  picture: PropTypes.string,
  numberOfTracks: PropTypes.number.isRequired,
  rank: PropTypes.number,
  createdOn: PropTypes.string,
  setPlaylist: PropTypes.func.isRequired,
  duration: PropTypes.number,
};

PlaylistMainInfo.defaultProps = {
  id: null,
  userId: null,
  username: null,
  title: null,
  picture: null,
  rank: null,
  createdOn: null,
  duration: null,
};

export default PlaylistMainInfo;
