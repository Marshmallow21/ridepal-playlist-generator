import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container, Grid, TextField, Button,
  Avatar, Typography, Link
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import _ from 'lodash';

import userService from 'services/user';
import formControls from 'common/form-controls';
import utils from 'common/utils';
import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  linkItem: {
    textAlign: 'end'
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = () => {
  const classes = useStyles();
  const history = useHistory();

  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const [loading, setLoading] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState(_.cloneDeep(formControls.REGISTER_FORM_CONTROLS));

  const formSubmitHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    userService.register(form.username.value, form.email.value,
      form.password.value)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else if (res.errors) {
          const message = Object
            .entries(res.errors)
            .map(error => `${error[0]}: ${error[1]}`)
            .join('\n');

          setNotification({
            isOpen: true,
            type: 'error',
            message,
          });
        } else {
          history.push('/login');
        }
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  };

  return (
    <Container disableGutters maxWidth="xs">
      <Grid
        container
        spacing={2}
        component='form'
        className='resetContainerWithSpacing'
      >
        <Grid item container xs={12} direction='column' alignItems='center'>
          <Grid item>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
          </Grid>
          <Grid item>
            <Typography component="h1" variant="h5">
              Register
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name={form.username.name}
            placeholder={form.username.placeholder}
            value={form.username.value}
            variant="outlined"
            error={form.username.touched && !form.username.valid}
            required
            fullWidth
            label={form.username.touched
              && !form.username.valid
              ? 'Error'
              : ''}
            helperText={form.username.touched
              && !form.username.valid
              ? `Username should be one word between 8-20 characters. Incldes a number, 
              alphanumeric characters, _ or .`
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name={form.email.name}
            placeholder={form.email.placeholder}
            value={form.email.value}
            variant="outlined"
            error={form.email.touched && !form.email.valid}
            required
            fullWidth
            label={form.email.touched
              && !form.email.valid
              ? 'Error'
              : ''}
            helperText={form.email.touched
              && !form.email.valid
              ? 'Expected email to be valid email with length of at least 7 symbols.'
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}

          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name={form.password.name}
            placeholder={form.password.placeholder}
            value={form.password.value}
            type="password"
            variant="outlined"
            error={form.password.touched && !form.password.valid}
            required
            fullWidth
            label={form.password.touched
              && !form.password.valid
              ? 'Error'
              : ''}
            helperText={form.password.touched
              && !form.password.valid
              ? `Password should be with length [8-20] and contain at least one
              number, one lowercase letter, one capital letter and one special symbol.`
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name={form.repeatPassword.name}
            placeholder={form.repeatPassword.placeholder}
            value={form.repeatPassword.value}
            type="password"
            variant="outlined"
            error={form.repeatPassword.touched
               && !form.repeatPassword.valid
               && form.password.value !== form.repeatPassword.value}
            required
            fullWidth
            label={form.repeatPassword.touched
              && !form.repeatPassword.valid
              && form.password.value !== form.repeatPassword.value
              ? 'Error'
              : ''}
            helperText={form.repeatPassword.touched
              && !form.repeatPassword.valid
              && form.password.value !== form.repeatPassword.value
              ? 'Repeat Password doesn\'t match Password'
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid, form.password.value);
            }}
          />
        </Grid>
        {loading && <Loading />}
        <Grid item xs={12}>
          <Button
            variant="contained"
            disabled={!isFormValid || !Object.values(form).some(
              (control) => control.touched
            )}
            color="secondary"
            fullWidth
            className={classes.submit}
            onClick={formSubmitHandler}
          >
            Register
          </Button>
        </Grid>
        <Grid item xs={12} className={classes.linkItem}>
          <Link href="/login" variant="body2">
            Already have an account? Sign in
          </Link>
        </Grid>
      </Grid>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </Container>
  );
};

export default SignUp;
