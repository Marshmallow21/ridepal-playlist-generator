import {
  Grid
} from '@material-ui/core';

const NoMatch = () => (
  <Grid
    item
    container
    justify="center"
  >
    <img src="/images/404-error-page-not-found.jpg" alt="ERROR 404 - page not found" />
  </Grid>
);

export default NoMatch;
