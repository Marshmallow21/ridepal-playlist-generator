/* eslint-disable camelcase */
import React from 'react';
import AdminTableHead from 'components/Admin/AdminTableHead';
import PropTypes from 'prop-types';

const headCells = [
  {
    id: 'title', numeric: false, icon: false, disablePadding: false, label: 'Title'
  },
  {
    id: 'playtime', numeric: true, icon: false, disablePadding: false, label: 'Playtime'
  },
  {
    id: 'rank', numeric: true, icon: false, disablePadding: false, label: 'Rank'
  },
  {
    id: 'genres', numeric: false, icon: false, disablePadding: false, label: 'Genres'
  },
  {
    id: 'created_on', numeric: false, icon: false, disablePadding: false, label: 'Created on'
  },
  {
    id: 'editPlaylist', numeric: false, icon: true, disablePadding: false, label: 'Edit'
  },
  {
    id: 'deletePlaylist', numeric: false, icon: true, disablePadding: false, label: 'Delete'
  },
  {
    id: 'username', numeric: false, icon: false, disablePadding: false, label: 'Created by'
  },
  {
    id: 'infoUser', numeric: false, icon: true, disablePadding: false, label: 'User'
  },
  {
    id: 'infoPlaylist', numeric: false, icon: true, disablePadding: false, label: 'Playlist'
  },
];

const AdminPlaylistsTableHead = (
  {
    classes, order, orderBy, numSelected, rowCount, onRequestSort
  }
) => (
  <AdminTableHead
    classes={classes}
    order={order}
    orderBy={orderBy}
    numSelected={numSelected}
    rowCount={rowCount}
    headCells={headCells}
    onRequestSort={onRequestSort}
  />

);

AdminPlaylistsTableHead.propTypes = {
  classes: PropTypes.objectOf.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

export default AdminPlaylistsTableHead;
