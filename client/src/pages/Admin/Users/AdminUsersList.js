/* eslint-disable camelcase */
import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import adminService from 'services/admin';
import { DateTime } from 'luxon';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Loading from 'components/Loading/Loading';
import Checkbox from '@material-ui/core/Checkbox';
import {
  IconButton, Tooltip, Container, Paper
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import CreateIcon from '@material-ui/icons/Create';
import AdminUsersTableHead from 'pages/Admin/Users/AdminUsersTableHead';
import AdminUsersTableToolbar from 'components/Admin/AdminTableToolbar';
import AdminTablePagination from 'components/Admin/AdminTablePagination';
import AdminTableDense from 'components/Admin/AdminTableDense';
import AdminEditUser from 'pages/Admin/Users/AdminEditUser';
import utils from 'common/utils';
import constants from 'common/constants.js';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  tableHead: {
    fontWeight: 'bold',
  }
}));

const AdminUsersList = () => {
  const history = useHistory();
  const classes = useStyles();
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('role');
  const [selected, setSelected] = useState();
  const [page, setPage] = useState(0);
  const [dense, setDense] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const [rows, setRows] = useState([]);

  const [isEditFormOpen, setEditFormOpen] = useState(false);

  const [userId, setUserId] = useState();
  const [user, setUser] = useState({});
  const [hasChanges, setHasChanges] = useState(false);

  const optionsRole = useRef(['All Users', 'Admin', 'User']);
  const optionsPlaylist = useRef(['All Playlists', 'At least one', 'Less than 10', 'More than 10']);
  const [filter, setFilter] = useState({});

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!loading) {
      setLoading(true);
      adminService.getAllUsers(filter)
        .then((result) => {
          setRows(result.data);
          setPage(0);
        })
        .catch(() => history.push('/500'))
        .finally(() => setLoading(false));
    }
  }, [hasChanges, filter]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleFormClose = () => setEditFormOpen(false);
  const isSelected = (id) => selected === id;

  const handleClick = (event, id) => {
    if (event.target.type === 'checkbox') {
      if (id === selected) {
        setSelected();
      } else {
        setSelected(id);
      }
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const handleProfile = (event, id, username) => {
    history.push({ pathname: `/profile/${username}`, state: id });
  };

  const handleEditProfile = (event, id, row = {}) => {
    setUserId(id);
    setUser(row);
    setEditFormOpen(true);
  };
  return (
    <Container
      disableGutters
      maxWidth="lg"
    >
      <Paper className={classes.paper}>
        <AdminUsersTableToolbar
          numSelected={selected ? 1 : 0}
          rows={rows}
          setRows={setRows}
          selected={selected}
          setSelected={setSelected}
          optionsRole={optionsRole.current}
          optionsPlaylist={optionsPlaylist.current}
          setFilter={setFilter}
          filter={filter}
          label='Users'
        />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <AdminUsersTableHead
              classes={classes}
              numSelected={selected && 1}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              setPage={setPage}
            />
            <TableBody>
              {loading && <Loading />}
              {utils.stableSort(rows, utils.getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.id);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.id)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none" align="right">
                        {row.id}
                      </TableCell>
                      <TableCell className={classes.tableHead} align="left">{row.username}</TableCell>
                      <TableCell align="left">{row.role}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{ DateTime.fromISO(row.created_on).toFormat('dd LLL yyyy') }</TableCell>
                      <TableCell align="center">{row.numberOfPlaylists}</TableCell>
                      <TableCell align="center">
                        <Tooltip
                          title="Edit profile"
                          arrow
                        >
                          <IconButton
                            className={classes.control}
                            edge="start"
                            onClick={(event) => handleEditProfile(event, row.id, row)}
                          >
                            <CreateIcon color='secondary' />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                      <TableCell align="center">
                        <Tooltip
                          title="View profile"
                          arrow
                        >
                          <IconButton
                            className={classes.control}
                            edge="start"
                            onClick={(event) => handleProfile(event, row.id, row.username)}
                          >
                            <InfoIcon color='primary' />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <AdminTablePagination
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rowsPerPageOptions={constants.ROWS_PER_PAGE}
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <AdminTableDense
        setDense={setDense}
        dense={dense}
      />
      <AdminEditUser
        isOpen={isEditFormOpen}
        handleClose={handleFormClose}
        userId={userId}
        user={user}
        setHasChanges={setHasChanges}
        hasChanges={hasChanges}
      />
    </Container>
  );
};

export default AdminUsersList;
