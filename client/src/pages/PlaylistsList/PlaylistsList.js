import { useState, useEffect, useContext } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import TuneIcon from '@material-ui/icons/Tune';

import Loading from 'components/Loading/Loading';
import CustomPagination from 'components/Pagination/CustomPagination';
import ItemsPerPage from 'components/Pagination/ItemsPerPage';
import PlaylistSimple from 'components/Playlist/PlaylistSimple';
import FiltersBar from 'components/PlaylistsFilters/FiltersBar';
import playlistsService from 'services/playlists';
import { FiltersContext } from 'providers/FiltersContext';

const PlaylistsList = () => {
  const history = useHistory();
  const location = useLocation();

  const {
    titleFilter,
    setTitleFilter,
    genresFilter,
    setGenresFilter,
    playtimeFilter,
    setPlaytimeFilter,
    topPlaylistsFilter,
    setTopPlaylistsFilter,
    sortBy,
    setSortBy,
    isFilterApplied,
    setIsFilterApplied,
    resetFiltersContext,
  } = useContext(FiltersContext);
  const [playlists, setPlaylists] = useState([]);

  const [page, setPage] = useState(1);
  const [totalNumberOfPlaylists, setTotalNumberOfPlaylists] = useState(0);
  const [numberOfPages, setNumberOfPages] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(12);
  const [isFiltersBarOpen, setFiltersBarOpen] = useState(false);
  const [isSearchTriggered, setIsSearchTriggered] = useState(false);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    resetFiltersContext();
  }, []);

  useEffect(() => {
    const urlParams = new URLSearchParams(location.search);

    // eslint-disable-next-line no-restricted-syntax
    for (const p of urlParams) {
      const [param, value] = p;
      switch (param) {
        case 'title':
          setTitleFilter(value);
          break;
        case 'playtime':
          setPlaytimeFilter(value
            .split(',')
            .reduce((o, key) => ({ ...o, [key]: true }), {}));
          break;
        case 'topTracks':
          setTopPlaylistsFilter(value);
          break;
        case 'genres':
          setGenresFilter(value
            .split(',')
            .reduce((o, key) => ({ ...o, [key]: true }), {}));
          break;
        case 'sortBy':
          setSortBy(value);
          break;
        default:
          break;
      }
    }

    setIsSearchTriggered(true);

    if (urlParams.toString().length > 0) {
      setIsFilterApplied(true);
    }
  }, []);

  useEffect(() => {
    if (isSearchTriggered) {
      setLoading(true);

      const urlParams = new URLSearchParams();
      const queryParams = {
        limit: itemsPerPage,
        offset: (page - 1) * itemsPerPage
      };

      if (titleFilter) {
        queryParams.title = titleFilter;
        urlParams.append('title', titleFilter);
      }

      const playtimeQueries = Object
        .keys(playtimeFilter).filter(key => playtimeFilter[key]);

      if (playtimeQueries.length) {
        queryParams.playtime = playtimeQueries;
        urlParams.append('playtime', playtimeQueries);
      }

      if (topPlaylistsFilter) {
        queryParams.topPlaylists = topPlaylistsFilter;
        urlParams.append('topTracks', topPlaylistsFilter);
      }

      const genresQueries = Object
        .keys(genresFilter).filter(key => genresFilter[key]);

      if (genresQueries.length) {
        queryParams.genres = genresQueries;
        urlParams.append('genres', genresQueries);
      }

      if (sortBy) {
        queryParams.sortBy = sortBy;
        urlParams.append('sortBy', sortBy);
      }

      if (page) {
        urlParams.append('page', page);
      }

      playlistsService.getAllPlaylists(queryParams)
        .then((result) => {
          setTotalNumberOfPlaylists(Number(result.total));
          setNumberOfPages(Math
            .ceil(Number(result.total) / Number(itemsPerPage)));
          setPlaylists(result.data);
        })
        .catch(() => history.push('/500'))
        .finally(() => {
          setLoading(false);
          setIsSearchTriggered(false);
        });

      history.push({ search: urlParams.toString() });
    }
  }, [page, itemsPerPage, isFilterApplied, isSearchTriggered]);

  const handleItemsPerPage = (limit) => {
    setPage(1);
    setItemsPerPage(limit);
    setIsSearchTriggered(true);
  };

  const openFiltersBar = () => {
    setFiltersBarOpen(true);
  };
  const closeFiltersBar = () => {
    setFiltersBarOpen(false);
  };

  return (
    <Container
      disableGutters
      maxWidth="lg"
    >
      <Grid
        container
        spacing={2}
        className='resetContainerWithSpacing'
      >
        <Grid
          item
          container
          spacing={1}
          alignItems='center'
          justify="flex-end"
        >
          <Grid item>
            <IconButton
              color="primary"
              onClick={openFiltersBar}
            >
              <TuneIcon
                fontSize="large"
              />
            </IconButton>
          </Grid>
          <Grid item>
            <ItemsPerPage
              itemsPerPage={itemsPerPage}
              setItemsPerPage={handleItemsPerPage}
            />
          </Grid>
        </Grid>
        <FiltersBar
          open={isFiltersBarOpen}
          setOpen={setFiltersBarOpen}
          closeFiltersBar={closeFiltersBar}
          setIsSearchTriggered={setIsSearchTriggered}
          setPage={setPage}
        />
        {loading && <Loading />}
        <Grid
          item
          container
          spacing={2}
        >
          {playlists.map((playlist) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={3}
              key={playlist.id}
            >
              <PlaylistSimple
                {...playlist}
              />
            </Grid>
          ))}
        </Grid>
        <Grid
          item
          container
          spacing={1}
          direction='column'
          alignItems='center'
        >
          <Grid item>
            <Typography variant='subtitle1'>
              {`Total number of playlists: ${totalNumberOfPlaylists}`}
            </Typography>
          </Grid>
          <Grid item>
            <CustomPagination
              numberOfPages={numberOfPages}
              page={page}
              setPage={setPage}
              setIsSearchTriggered={setIsSearchTriggered}
            />
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

export default PlaylistsList;
