import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Carousel from 'react-multi-carousel';
import PropTypes from 'prop-types';
import 'react-multi-carousel/lib/styles.css';
import { Grid, Typography } from '@material-ui/core';
import playlistsService from 'services/playlists.js';
import Box from '@material-ui/core/Box';
import PlaylistSimple from 'components/Playlist/PlaylistSimple';
import Loading from 'components/Loading/Loading';
import 'pages/Home/Carousel.css';

const responsive = {
  desktop: {
    breakpoint: { max: 4000, min: 1024 },
    items: 5,
    slidesToSlide: 5,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
    slidesToSlide: 3
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1,
  }
};

const PlaylistsSection = ({ className }) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [playlists, setPlaylists] = useState([]);

  useEffect(() => {
    setLoading(true);
    playlistsService
      .getAllPlaylists({ limit: 10, sortBy: 'rank.desc' })
      .then((res) => setPlaylists(res.data))
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, []);

  return (
    <>
      <Grid
        item
        container
        justify="center"
        xs={12}
      >
        <Box mb={2} mt={6}>
          <Typography
            variant="h5"
            className={className}
          >
            Discover our top rated playlists
          </Typography>
        </Box>
      </Grid>
      { loading && <Loading />}
      <Grid item>
        <Carousel
          infinite
          responsive={responsive}
          arrows
          itemClass="carousel-item"
        >
          {playlists.map((playlist) => (
            <PlaylistSimple
              key={playlist.id}
              {...playlist}
            />
          ))}
        </Carousel>
      </Grid>
    </>
  );
};

PlaylistsSection.propTypes = {
  className: PropTypes.string.isRequired,
};

export default PlaylistsSection;
