import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';

import artistsService from 'services/artists.js';
import ArtistSimple from 'components/Artist/ArtistSimple';
import Loading from 'components/Loading/Loading';
import 'pages/Home/Carousel.css';

const responsive = {
  desktop: {
    breakpoint: { max: 4000, min: 1360 },
    items: 6,
    slidesToSlide: 6,
  },
  laptop: {
    breakpoint: { max: 1360, min: 1160 },
    items: 5,
    slidesToSlide: 5
  },
  tablet1: {
    breakpoint: { max: 1160, min: 920 },
    items: 4,
    slidesToSlide: 4
  },
  tablet2: {
    breakpoint: { max: 920, min: 700 },
    items: 3,
    slidesToSlide: 3
  },
  mobile1: {
    breakpoint: { max: 700, min: 530 },
    items: 2,
    slidesToSlide: 2,
  },
  mobile2: {
    breakpoint: { max: 530, min: 0 },
    items: 1,
    slidesToSlide: 1,
  }
};

const ArtistsSection = ({ className }) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [artists, setArtists] = useState([]);

  useEffect(() => {
    setLoading(true);
    artistsService.getAllArtists()
      .then((res) => setArtists(res))
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, []);

  return (
    <>
      <Grid
        item
        container
        justify="center"
        xs={12}
      >
        <Box mb={4}>
          <Typography
            variant="h5"
            color="textPrimary"
            className={className}
          >
            {`Music from 
          ${artists.length}
          rock artists`}
          </Typography>
        </Box>
      </Grid>
      { loading && <Loading />}
      <Grid item>
        <Carousel
          infinite
          responsive={responsive}
          arrows
          itemClass="carousel-item"
        >
          {artists.map((artist) => (
            <ArtistSimple
              key={artist.id}
              {...artist}
            />
          ))}
        </Carousel>
      </Grid>
    </>
  );
};

ArtistsSection.propTypes = {
  className: PropTypes.string.isRequired,
};
export default ArtistsSection;
