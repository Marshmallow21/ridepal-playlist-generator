import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(5),
  },
  logo: {
    width: theme.spacing(12),
    height: theme.spacing(12),
  },
}));

const Confirmation = ({ error }) => {
  const classes = useStyles();

  return (
    <Grid
      item
      container
      spacing={6}
      direction='column'
      alignItems='center'
      justify='center'
      wrap='nowrap'
      className={classes.container}
    >
      <Grid
        item
        container
        justify="center"
      >
        <img
          src='/images/logo-dark.png'
          alt="logo"
          className={classes.logo}
        />
      </Grid>
      {!error
        ? (
          <>
            <Typography
              variant="h4"
              gutterBottom
              align="center"
            >
              Ready to get your trip playlist? Generate now or go back and adjust your answers!
            </Typography>
            <Typography
              variant="h6"
              gutterBottom
              align="center"
            >
              Your playlist will be matched to the length of your ride and your preferences.
            </Typography>
          </>
        ) : (
          <>
            <Typography
              variant="h4"
              gutterBottom
              align="center"
            >
              {error}
            </Typography>
            <Typography
              variant="h6"
              gutterBottom
              align="center"
            >
              Please, go back and adjust your playlist preferences or try again later.
            </Typography>
          </>
        )}
    </Grid>
  );
};

Confirmation.propTypes = {
  error: PropTypes.string.isRequired,
};

export default Confirmation;
