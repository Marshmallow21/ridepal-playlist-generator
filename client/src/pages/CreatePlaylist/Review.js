import { useHistory } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import PlaylistSimple from 'components/Playlist/PlaylistSimple';

const Review = ({ playlist }) => {
  const history = useHistory();

  return (
    <Grid
      container
      spacing={2}
      direction='column'
      justify='center'
      alignItems="center"
      wrap='nowrap'
    >
      <Grid item>
        <Typography
          variant="h6"
          gutterBottom
          align="center"
        >
          You are ready to rock your trip!
        </Typography>
      </Grid>
      <Grid
        item
        container
        justify="center"
        xs={12}
        sm={6}
        md={3}
      >
        <PlaylistSimple {...playlist} />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => history.push(`/playlists/${playlist.id}`)}
        >
          Go to playlist
        </Button>
      </Grid>
    </Grid>
  );
};

Review.propTypes = {
  playlist: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    playtime: PropTypes.number.isRequired,
    rank: PropTypes.number.isRequired,
    picture: PropTypes.string.isRequired,
  }).isRequired,
};

export default Review;
