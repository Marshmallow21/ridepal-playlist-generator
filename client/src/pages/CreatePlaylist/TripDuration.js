import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Grid, Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import FlightIcon from '@material-ui/icons/Flight';
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike';
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk';
import DriveEtaIcon from '@material-ui/icons/DriveEta';

import PropTypes from 'prop-types';
import utils from 'common/utils';
import { Duration } from 'luxon';

const useStyles = makeStyles((theme) => ({
  durationInput: {
    fontSize: theme.spacing(3.5),
    textAlign: 'center',
  },
  durationIcon: {
    fontSize: theme.spacing(6),
  }
}));

const TripDuration = ({
  form, setForm, setIsFormValid,
  durationTrip
}) => {
  const classes = useStyles();

  const airplane = durationTrip / 6.55;
  const bike = durationTrip / 0.25;
  const walk = durationTrip / 0.058;

  const handleClickedCar = () => {
    utils.setFormControlValue('duration', Duration.fromMillis((durationTrip * 1000)).toFormat('hh:mm:ss'), form, setForm, setIsFormValid);
  };
  const handleClickedWalk = () => {
    utils.setFormControlValue('duration', Duration.fromMillis((walk * 1000)).toFormat('hh:mm:ss'), form, setForm, setIsFormValid);
  };
  const handleClickedBike = () => {
    utils.setFormControlValue('duration', Duration.fromMillis((bike * 1000)).toFormat('hh:mm:ss'), form, setForm, setIsFormValid);
  };
  const handleClickedPlane = () => {
    utils.setFormControlValue('duration', Duration.fromMillis((airplane * 1000)).toFormat('hh:mm:ss'), form, setForm, setIsFormValid);
  };
  return (

    <Grid
      container
      spacing={2}
      direction='column'
      justify='center'
      wrap='nowrap'
    >
      <Grid item>
        <Typography
          variant="h6"
          gutterBottom
          align="center"
        >
          Confirm trip duration
        </Typography>
      </Grid>
      <Grid
        container
        item
        direction="row"
        justify="center"
        alignItems="center"
        spacing={2}
      >
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={handleClickedCar}
            startIcon={<DriveEtaIcon />}
          >
            {Duration
              .fromMillis((durationTrip * 1000))
              .toFormat('hh:mm:ss')}
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={handleClickedWalk}
            startIcon={<DirectionsWalkIcon />}
          >
            {Duration
              .fromMillis((walk * 1000))
              .toFormat('hh:mm:ss')}
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={handleClickedBike}
            startIcon={<DirectionsBikeIcon />}
          >
            {Duration
              .fromMillis((bike * 1000))
              .toFormat('hh:mm:ss')}
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={handleClickedPlane}
            startIcon={<FlightIcon />}
          >
            {Duration
              .fromMillis((airplane * 1000))
              .toFormat('hh:mm:ss')}
          </Button>
        </Grid>
      </Grid>

      <Grid
        item
        container
        justify="center"
        alignItems="center"
        direction="column"
      >
        <Grid item>
          <AccessTimeIcon
            className={classes.durationIcon}
            color="secondary"
          />
        </Grid>
        <Grid item>
          <TextField
            variant="outlined"
            name={form.duration.name}
            placeholder={form.duration.placeholder}
            value={form.duration.value}
            type="text"
            error={form.duration.touched
              && !form.duration.valid}
            required
            label={form.duration.touched
              && !form.duration.valid
              ? 'Error'
              : ''}
            helperText={form.duration.touched
              && !form.duration.valid
              ? 'Duration should be between 5 minutes and 24 hours.'
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}
            InputProps={{
              classes: {
                input: classes.durationInput,
              },
            }}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

TripDuration.propTypes = {
  form: PropTypes.shape({
    duration: PropTypes.shape({
      name: PropTypes.string.isRequired,
      placeholder: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      validation: PropTypes.shape({
        required: PropTypes.bool.isRequired,
        minDuration: PropTypes.number.isRequired,
        maxDuration: PropTypes.number.isRequired,
        regex: PropTypes.instanceOf(RegExp).isRequired,
      }).isRequired,
      valid: PropTypes.bool.isRequired,
      touched: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  setForm: PropTypes.func.isRequired,
  setIsFormValid: PropTypes.func.isRequired,
  durationTrip: PropTypes.number.isRequired
};

export default TripDuration;
