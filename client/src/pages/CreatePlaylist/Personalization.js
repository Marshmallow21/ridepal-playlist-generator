import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import unsplashService from 'services/unsplash';
import utils from 'common/utils';

const useStyles = makeStyles((theme) => ({
  pictureContainer: {
    '&:hover > $overlay': {
      display: 'block',
    },
    '&:hover > $picture': {
      filter: 'brightness(60%)',
    },
    position: 'relative',
    width: theme.spacing(30),
    height: theme.spacing(30),
  },
  picture: {
    position: 'absolute',
    top: '0%',
    left: '0%',
    width: theme.spacing(30),
    height: theme.spacing(30),
  },
  overlay: {
    display: 'none',
    position: 'absolute',
    width: '100%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}));

const Personalization = ({
  form, setForm, setIsFormValid
}) => {
  const classes = useStyles();

  const setRandomPicture = () => {
    unsplashService.getRandomRockImage()
      .then(res => {
        utils.setFormControlImageURL('picture', res.urls.regular, form, setForm, setIsFormValid);
      });
  };

  return (
    <Grid
      container
      spacing={6}
      direction='column'
      justify='center'
      alignItems="center"
      wrap='nowrap'
    >
      <Grid item>
        <Typography
          variant="h6"
          gutterBottom
          align="center"
        >
          Add playlist title and cover
        </Typography>
      </Grid>
      <Grid
        item
        md={6}
        container
        spacing={4}
        justify="center"
        alignItems="center"
        component="form"
      >
        <Grid
          item
          className={classes.pictureContainer}
        >
          <Avatar
            variant="rounded"
            className={classes.picture}
            src={form.picture.preview}
            alt={form.picture.name}
          />
          <Grid
            item
            className={classes.overlay}
          >
            <Grid
              item
              container
              justify="center"
              spacing={2}
            >
              <Grid item>
                {/* eslint-disable jsx-a11y/label-has-associated-control */}
                <input
                  id="image-file-input"
                  name={form.picture.name}
                  type="file"
                  accept="image/png, image/jpeg"
                  onChange={(e) => {
                    utils.handleInputChange(e, form, setForm, setIsFormValid);
                  }}
                  style={{ display: 'none', }}
                />
                <label htmlFor="image-file-input">
                  <Button
                    variant="contained"
                    color="secondary"
                    component="span"
                  >
                    Upload cover
                  </Button>
                </label>
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={setRandomPicture}
                >
                  Or set random
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {(form.picture.touched
          && !form.picture.valid)
          && (
            <Grid item>
              <FormHelperText error>
                Picture should be in .png, .jpg or .jpeg.
              </FormHelperText>
            </Grid>
          )}
        <Grid item>
          <TextField
            variant="outlined"
            name={form.title.name}
            placeholder={form.title.placeholder}
            value={form.title.value}
            error={form.title.touched && !form.title.valid}
            required
            fullWidth
            label={form.title.touched
              && !form.title.valid
              ? 'Error'
              : ''}
            helperText={form.title.touched
              && !form.title.valid
              ? 'Title should be between 1 and 255 characters.'
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

Personalization.propTypes = {
  form: PropTypes.shape({
    title: PropTypes.shape({
      name: PropTypes.string.isRequired,
      placeholder: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      validation: PropTypes.shape({
        required: PropTypes.bool.isRequired,
        minLength: PropTypes.number.isRequired,
        maxLength: PropTypes.number.isRequired,
      }).isRequired,
      valid: PropTypes.bool.isRequired,
      touched: PropTypes.bool.isRequired,
    }).isRequired,
    picture: PropTypes.shape({
      name: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      files: PropTypes.objectOf(PropTypes.any),
      imageURL: PropTypes.string.isRequired,
      preview: PropTypes.string.isRequired,
      validation: PropTypes.shape({
        required: PropTypes.bool.isRequired,
        validFileTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
      }).isRequired,
      valid: PropTypes.bool.isRequired,
      touched: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  setForm: PropTypes.func.isRequired,
  setIsFormValid: PropTypes.func.isRequired,
};

export default Personalization;
