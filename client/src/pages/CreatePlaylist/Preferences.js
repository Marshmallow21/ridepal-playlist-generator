import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import utils from 'common/utils';

const Preferences = ({
  form, setForm, setIsFormValid
}) => (
  <Grid
    container
    spacing={6}
    direction='column'
    justify='center'
    alignItems="center"
    wrap='nowrap'
  >
    <Grid item>
      <Typography
        variant="h6"
        gutterBottom
        align="center"
      >
        Adjust preferences
      </Typography>
    </Grid>
    <Grid
      item
      container
      sm={8}
      md={4}
      alignItems="center"
      direction="column"
    >
      <Grid
        container
        justify="space-between"
        alignItems="center"
      >
        <Grid item>
          <Typography
            variant="h6"
            color="primary"
          >
            {form.repeatArtists.placeholder}
          </Typography>
        </Grid>
        <Grid item>
          <FormControlLabel
            control={(
              <Switch
                edge="end"
                onChange={() => {
                  utils.setFormControlValue('repeatArtists',
                    form.repeatArtists.value ? '' : 'on',
                    form,
                    setForm,
                    setIsFormValid);
                }}
                checked={!!form.repeatArtists.value}
              />
            )}
          />
        </Grid>
      </Grid>
      <Grid
        container
        justify="space-between"
        alignItems="center"
      >
        <Grid item>
          <Typography
            variant="h6"
            color="primary"
          >
            {form.topTracks.placeholder}
          </Typography>
        </Grid>
        <Grid item>
          <FormControlLabel
            control={(
              <Switch
                edge="end"
                onChange={() => {
                  utils.setFormControlValue('topTracks',
                    form.topTracks.value ? '' : 'on',
                    form,
                    setForm,
                    setIsFormValid);
                }}
                checked={!!form.topTracks.value}
              />
            )}
          />
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);

Preferences.propTypes = {
  form: PropTypes.shape({
    repeatArtists: PropTypes.shape({
      placeholder: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      validation: PropTypes.shape({
        required: PropTypes.bool.isRequired,
      }).isRequired,
      valid: PropTypes.bool.isRequired,
      touched: PropTypes.bool.isRequired,
    }).isRequired,
    topTracks: PropTypes.shape({
      placeholder: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      validation: PropTypes.shape({
        required: PropTypes.bool.isRequired,
      }).isRequired,
      valid: PropTypes.bool.isRequired,
      touched: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  setForm: PropTypes.func.isRequired,
  setIsFormValid: PropTypes.func.isRequired,
};

export default Preferences;
