import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import _ from 'lodash';
import PropTypes from 'prop-types';

import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';
import formControls from 'common/form-controls';
import userService from 'services/user';
import utils from 'common/utils';

const UserPasswordForm = ({
  isOpen, handleClose, user
}) => {
  const history = useHistory();

  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState(_.cloneDeep(formControls.USER_PASSWORD_FORM_CONTROLS));

  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const formSubmitHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    userService.changePassword(user.id, form.newPassword.value, form.oldPassword.value)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else if (res.errors) {
          const message = Object
            .entries(res.errors)
            .map(error => `${error[0]}: ${error[1]}`)
            .join('\n');

          setNotification({
            isOpen: true,
            type: 'error',
            message,
          });
        } else {
          setNotification({
            isOpen: true,
            type: 'success',
            message: 'Successfully updated your password.'
          });
        }

        setForm(_.cloneDeep(formControls.USER_PASSWORD_FORM_CONTROLS));
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  };

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>Change password</DialogTitle>
        <DialogContent>
          <Grid container spacing={2} component='form'>
            <Grid item xs={12}>
              <TextField
                name={form.newPassword.name}
                placeholder={form.newPassword.placeholder}
                value={form.newPassword.value}
                type="password"
                variant="outlined"
                error={form.newPassword.touched && !form.newPassword.valid}
                required
                fullWidth
                label={form.newPassword.touched
                  && !form.newPassword.valid
                  ? 'Error'
                  : ''}
                helperText={form.newPassword.touched
                  && !form.newPassword.valid
                  ? `Password should be with length [8-20] and contain at least one
              number, one lowercase letter, one capital letter and one special symbol.`
                  : ''}
                onChange={(e) => {
                  utils.handleInputChange(e, form, setForm, setIsFormValid);
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name={form.oldPassword.name}
                placeholder={form.oldPassword.placeholder}
                value={form.oldPassword.value}
                type="password"
                variant="outlined"
                error={form.oldPassword.touched && !form.oldPassword.valid}
                required
                fullWidth
                label={form.oldPassword.touched
                  && !form.oldPassword.valid
                  ? 'Error'
                  : ''}
                helperText={form.oldPassword.touched
                  && !form.oldPassword.valid
                  ? `Password should be with length [8-20] and contain at least one
              number, one lowercase letter, one capital letter and one special symbol.`
                  : ''}
                onChange={(e) => {
                  utils.handleInputChange(e, form, setForm, setIsFormValid);
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name={form.repeatPassword.name}
                placeholder={form.repeatPassword.placeholder}
                value={form.repeatPassword.value}
                type="password"
                variant="outlined"
                error={form.repeatPassword.touched
                  && !form.repeatPassword.valid
                  && form.oldPassword.value !== form.repeatPassword.value}
                required
                fullWidth
                label={form.repeatPassword.touched
                  && !form.repeatPassword.valid
                  && form.oldPassword.value !== form.repeatPassword.value
                  ? 'Error'
                  : ''}
                helperText={form.repeatPassword.touched
                  && !form.repeatPassword.valid
                  && form.oldPassword.value !== form.repeatPassword.value
                  ? 'Should match previous password'
                  : ''}
                onChange={(e) => {
                  utils.handleInputChange(e, form, setForm, setIsFormValid, form.oldPassword.value);
                }}
              />
            </Grid>
          </Grid>
          {loading && <Loading />}
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            disabled={!isFormValid || !Object.values(form).some(
              (control) => control.touched
            )}
            color="secondary"
            onClick={(e) => {
              formSubmitHandler(e);
              handleClose();
            }}
          >
            Save
          </Button>
          <Button
            onClick={handleClose}
            color="primary"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </>
  );
};

UserPasswordForm.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number,
  }),
};

UserPasswordForm.defaultProps = {
  user: {
    id: null,
  },
};

export default UserPasswordForm;
