import { useContext, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import Avatar from '@material-ui/core/Avatar';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import _ from 'lodash';
import PropTypes from 'prop-types';

import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';
import { AuthContext } from 'providers/АuthContext';
import formControls from 'common/form-controls';
import userService from 'services/user';
import utils from 'common/utils';

const useStyles = makeStyles((theme) => ({
  avatarContainer: {
    '&:hover > $overlay': {
      display: 'block',
    },
    '&:hover > $avatar': {
      filter: 'brightness(60%)',
    },
    position: 'relative',
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  overlay: {
    display: 'none',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  icon: {
    color: '#FAFAFA',
    fontSize: theme.spacing(6),
  }
}));

const UserDetailsForm = ({
  isOpen, handleClose, user, setUser
}) => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useContext(AuthContext);

  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState(_.cloneDeep(formControls.USER_DETAILS_FORM_CONTROLS));

  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  useEffect(() => {
    if (Object.keys(user).length !== 0) {
      Object
        .entries(user)
        .forEach(prop => {
          const [key, value] = prop;

          if (form[key]) {
            const updatedControl = form[key];

            updatedControl.value = value;
            updatedControl.valid = true;

            if (key === 'avatar' && value) {
              updatedControl.preview = `/images/${value}`;
            }
          }
        });

      setForm({ ...form });
      setIsFormValid(true);
    }
  }, [user]);

  const formSubmitHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    if (form.username.value && form.username.value !== user.username) {
      formData.append('username', form.username.value);
    }

    if (form.email.value && form.email.value !== user.email) {
      formData.append('email', form.email.value);
    }

    if (form.avatar.files) {
      formData.append('image', form.avatar.files[0], form.avatar.files[0].filename);
    }

    setLoading(true);

    userService.updateUserDetails(user.id, formData)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else if (res.errors) {
          const message = Object
            .entries(res.errors)
            .map(error => `${error[0]}: ${error[1]}`)
            .join('\n');

          setNotification({
            isOpen: true,
            type: 'error',
            message,
          });
        } else {
          setNotification({
            isOpen: true,
            type: 'success',
            message: 'Successfully updated your profile.'
          });

          if (form.username.value) {
            setUser((prev) => ({ ...prev, username: res.username }));
          }

          if (form.avatar.files) {
            auth.setAvatar(res.avatar);
            setUser((prev) => ({ ...prev, avatar: res.avatar }));
          }
        }

        setForm(_.cloneDeep(formControls.USER_FORM_CONTROLS));
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  };

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>Profile details</DialogTitle>
        <DialogContent>
          <Grid
            container
            spacing={2}
            component='form'
          >
            <Grid item sm={4}>
              <Grid
                item
                className={classes.avatarContainer}
              >
                <Avatar
                  className={classes.avatar}
                  src={form.avatar.preview}
                  alt={form.avatar.name}
                />
                <Grid
                  item
                  className={classes.overlay}
                >
                  {/* eslint-disable jsx-a11y/label-has-associated-control */}
                  <input
                    id="image-file-input"
                    name={form.avatar.name}
                    type="file"
                    accept="image/png, image/jpeg"
                    onChange={(e) => {
                      utils.handleInputChange(e, form, setForm, setIsFormValid);
                    }}
                    style={{ display: 'none', }}
                  />
                  <label htmlFor="image-file-input">
                    <AddPhotoAlternateIcon className={classes.icon} />
                  </label>
                </Grid>
              </Grid>
              {(form.avatar.touched
                && !form.avatar.valid)
                && (
                  <Grid item>
                    <FormHelperText error>
                      avatar should be in .png, .jpg or .jpeg.
                    </FormHelperText>
                  </Grid>
                )}
            </Grid>
            <Grid
              item
              container
              alignItems="center"
              sm={8}
            >
              <Grid item xs={12}>
                <TextField
                  name={form.username.name}
                  placeholder={form.username.placeholder}
                  value={form.username.value}
                  variant="outlined"
                  error={form.username.touched && !form.username.valid}
                  multiline
                  required
                  fullWidth
                  label={form.username.touched
                    && !form.username.valid
                    ? 'Error'
                    : ''}
                  helperText={form.username.touched
                    && !form.username.valid
                    ? `Username should be between 8-20 characters and include
              alphanumeric characters, _ and . (non-consecutive).`
                    : ''}
                  onChange={(e) => {
                    utils.handleInputChange(e, form, setForm, setIsFormValid);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  name={form.email.name}
                  placeholder={form.email.placeholder}
                  value={form.email.value}
                  variant="outlined"
                  error={form.email.touched && !form.email.valid}
                  multiline
                  required
                  fullWidth
                  label={form.email.touched
                    && !form.email.valid
                    ? 'Error'
                    : ''}
                  helperText={form.email.touched
                    && !form.email.valid
                    ? 'Expected email to be valid email with length of at least 7 symbols.'
                    : ''}
                  onChange={(e) => {
                    utils.handleInputChange(e, form, setForm, setIsFormValid);
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
          {loading && <Loading />}
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            disabled={!isFormValid || !Object.values(form).some(
              (control) => control.touched
            )}
            color="secondary"
            onClick={(e) => {
              formSubmitHandler(e);
              handleClose();
            }}
          >
            Save
          </Button>
          <Button
            onClick={handleClose}
            color="primary"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </>
  );
};

UserDetailsForm.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number,
    username: PropTypes.string,
    email: PropTypes.string,
    avatar: PropTypes.string,
  }),
  setUser: PropTypes.func.isRequired,
};

UserDetailsForm.defaultProps = {
  user: {
    id: null,
    username: null,
    email: null,
    avatar: null,
  },
};

export default UserDetailsForm;
