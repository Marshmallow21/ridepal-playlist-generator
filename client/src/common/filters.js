export const genresList = {
  ROCK: 'Rock',
  METAL: 'Metal',
  ALTERNATIVE: 'Alternative',
};

export const sortByItems = {
  TITLE: 'title',
  PLAYTIME: 'playtime',
  RANK: 'rank',
};

export const sortDirection = {
  ASC: '.asc',
  DESC: '.desc',
};

export const playtimeWindows = [
  {
    min: 5 * 60,
    max: 30 * 60,
  },
  {
    min: 30 * 60 + 1,
    max: 60 * 60,
  },
  {
    min: 60 * 60 + 1,
    max: 2 * 60 * 60,
  },
  {
    min: 2 * 60 * 60 + 1,
    max: 5 * 60 * 60,
  },
  {
    min: 5 * 60 * 60 + 1,
    max: 10 * 60 * 60,
  },
  {
    min: 10 * 60 * 60 + 1,
    max: 15 * 60 * 60,
  },
  {
    min: 15 * 60 * 60 + 1,
    max: 24 * 60 * 60,
  },
];
