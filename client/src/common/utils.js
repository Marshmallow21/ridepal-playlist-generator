import { Duration } from 'luxon';

const isInputValid = (input, validations, files) => {
  let isValid = true;

  if (validations.required) {
    isValid = isValid && input.length !== 0;
  }

  if (validations.minLength) {
    isValid = isValid && input.length >= validations.minLength;
  }

  if (validations.maxLength) {
    isValid = isValid && input.length <= validations.maxLength;
  }

  if (validations.validFileTypes) {
    isValid = isValid && validations.validFileTypes.includes(files[0].type);
  }

  if (validations.regex) {
    isValid = isValid && input.match(validations.regex);
  }

  if (validations.minDuration) {
    isValid = isValid && (Duration.fromISOTime(input).valueOf() / 1000) >= validations.minDuration;
  }

  if (validations.maxDuration) {
    isValid = isValid && (Duration.fromISOTime(input).valueOf() / 1000) <= validations.maxDuration;
  }

  if (validations.valuesTotal) {
    isValid = isValid
      && Object.values(input).length !== 0
      && Object.values(input).reduce((acc, curr) => acc + curr) === 100;
  }

  if (!validations.required) {
    isValid = isValid || input === '';
  }

  return isValid;
};

const handleInputChange = (event, form, setForm, setIsFormValid, pass) => {
  const { name, value, files } = event.target;
  const updatedControl = { ...form[name] };

  updatedControl.value = value;

  if (files) {
    updatedControl.files = files;
    updatedControl.imageURL = '';
    updatedControl.preview = URL.createObjectURL(files[0]);
  }

  updatedControl.touched = true;

  if (name === 'repeatPassword' && updatedControl.value !== pass) {
    updatedControl.valid = false;
  } else {
    updatedControl.valid = isInputValid(value, updatedControl.validation, files);
  }

  const updatedForm = { ...form, [name]: updatedControl };
  setForm(updatedForm);

  const formValid = Object.values(updatedForm).every(
    (control) => control.valid
  );

  setIsFormValid(formValid);
};

const setFormControlValue = (control, value, form, setForm, setIsFormValid) => {
  const updatedControl = { ...form[control] };

  updatedControl.value = value;

  updatedControl.touched = true;
  updatedControl.valid = isInputValid(value, updatedControl.validation);

  const updatedForm = { ...form, [control]: updatedControl };
  setForm(updatedForm);

  const formValid = Object.values(updatedForm).every(
    (c) => c.valid
  );

  setIsFormValid(formValid);
};

const setFormControlImageURL = (control, url, form, setForm, setIsFormValid) => {
  const updatedControl = { ...form[control] };

  updatedControl.value = url;
  updatedControl.preview = url;
  updatedControl.imageURL = url;
  updatedControl.files = null;
  updatedControl.touched = true;
  updatedControl.valid = true;

  const updatedForm = { ...form, [control]: updatedControl };
  setForm(updatedForm);

  const formValid = Object.values(updatedForm).every(
    (c) => c.valid
  );

  setIsFormValid(formValid);
};

const descendingComparator = (a, b, orderBy) => {
  if (typeof a[orderBy] === 'string') {
    if (b[orderBy].toUpperCase() < a[orderBy].toUpperCase()) {
      return -1;
    }
    if (b[orderBy].toUpperCase() > a[orderBy].toUpperCase()) {
      return 1;
    }
    return 0;
  }
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};

const getComparator = (order, orderBy) => (order === 'desc'
  ? (a, b) => descendingComparator(a, b, orderBy)
  : (a, b) => -descendingComparator(a, b, orderBy));

const stableSort = (array, comparator) => {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
};

const pickColor = () => (`#${Math.floor(Math.random() * 16777214).toString(16).padStart(6, '0')}`);

const setDataPieCharts = (property, propertyId, result) => {
  const uniqueArray = Array.from(new Set(result.map(t => t[property])));
  return uniqueArray.map((el) => {
    const pieData = {};
    const color = pickColor();
    pieData.title = el;
    pieData.value = result.filter(o => o[property] === el).length;
    pieData.color = color;
    pieData[propertyId] = result.find(e => e[property] === el)[propertyId];
    return pieData;
  });
};

const sortRandom = (arr) => arr.sort(() => Math.random() - 0.5);

export default {
  descendingComparator,
  getComparator,
  stableSort,
  handleInputChange,
  setFormControlValue,
  setFormControlImageURL,
  pickColor,
  setDataPieCharts,
  sortRandom,
};
