import { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';
import { AuthContext } from 'providers/АuthContext';

const GuardedRoute = ({
  component: Component, admin, ...rest
}) => {
  const { user, setUser, setAvatar } = useContext(AuthContext);
  const renderComponent = (props) => {
    if (user) {
      const tokenTime = DateTime.fromSeconds(user.exp).toUTC().toMillis();
      const currentTime = DateTime.now().toUTC().toMillis();

      if (currentTime > tokenTime) {
        setUser(null);
        setAvatar(null);
        localStorage.removeItem('token');
        localStorage.setItem('exp', true);

        return <Redirect to='/login' />;
      }

      if (!admin || (admin && user.role === 'admin')) {
        return <Component {...props} />;
      }

      return <Redirect to='/home' />;
    }

    return <Redirect to='/login' />;
  };

  return (
    <Route
      {...rest}
      render={renderComponent}
    />
  );
};

GuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    role_id: PropTypes.number.isRequired,
    role: PropTypes.string.isRequired,
    exp: PropTypes.number.isRequired,
  }),
  admin: PropTypes.bool,
};

GuardedRoute.defaultProps = {
  user: null,
  admin: false,
};

export default GuardedRoute;
