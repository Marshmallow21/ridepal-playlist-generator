import { createContext, useState } from 'react';
import {
  genresList, sortByItems, sortDirection, playtimeWindows
} from 'common/filters';
import PropTypes from 'prop-types';

export const FiltersContext = createContext({
  titleFilter: null,
  setTitleFilter: () => {},
  genresFilter: null,
  setGenresFilter: () => {},
  playtimeFilter: 0,
  setPlaytimeFilter: () => {},
  topPlaylistsFilter: false,
  setTopPlaylistsFilter: () => {},
  sortBy: null,
  setSortBy: () => {},
  isFilterApplied: false,
  setIsFilterApplied: () => {},
  resetFiltersContext: () => {},
});

const FiltersContextProvider = ({ children }) => {
  const [titleFilter, setTitleFilter] = useState('');
  const [genresFilter, setGenresFilter] = useState(
    Object.values(genresList).reduce((o, key) => ({ ...o, [key]: false }), {})
  );
  const [playtimeFilter, setPlaytimeFilter] = useState(
    playtimeWindows.reduce((acc, el) => ({
      ...acc,
      [`${el.min}-${el.max}`]: false,
    }), {})
  );
  const [topPlaylistsFilter, setTopPlaylistsFilter] = useState(false);
  const [sortBy, setSortBy] = useState(`${sortByItems.RANK}${sortDirection.DESC}`);
  const [isFilterApplied, setIsFilterApplied] = useState(false);

  const resetFiltersContext = () => {
    setTitleFilter('');
    setGenresFilter(Object.values(genresList).reduce((o, key) => ({ ...o, [key]: false }), {}));
    setPlaytimeFilter(
      playtimeWindows.reduce((acc, el) => ({
        ...acc,
        [`${el.min}-${el.max}`]: false,
      }), {})
    );
    setTopPlaylistsFilter(false);
    setSortBy(`${sortByItems.RANK}${sortDirection.DESC}`);
    setIsFilterApplied(false);
  };

  return (
    <FiltersContext.Provider
      value={{
        titleFilter,
        setTitleFilter,
        genresFilter,
        setGenresFilter,
        playtimeFilter,
        setPlaytimeFilter,
        topPlaylistsFilter,
        setTopPlaylistsFilter,
        sortBy,
        setSortBy,
        isFilterApplied,
        setIsFilterApplied,
        resetFiltersContext,
      }}
    >
      {children}
    </FiltersContext.Provider>
  );
};

FiltersContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FiltersContextProvider;
