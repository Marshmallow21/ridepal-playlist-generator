import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#9E9E9E',
      main: '#36363A',
      dark: '#1F1F21',
      contrastText: '#FFF',
    },
    secondary: {
      light: '#FFF',
      main: '#E52139',
      dark: '#BC1A2d',
      contrastText: '#FFF',
    },
  },
  overrides: {
    MuiStepIcon: {
      root: {
        '&$completed': {
          color: '#E52139',
        },
        '&$active': {
          color: '#E52139',
        },
      },
    },
    MuiSlider: {
      rail: {
        height: 5,
        backgroundColor: '#9E9E9E',
      },
      track: {
        height: 5,
      },
      mark: {
        backgroundColor: '#E52139',
        height: 5,
        width: 4,
      },
      markActive: {
        backgroundColor: '#E52139',
        height: 5,
        width: 4,
      },
      thumb: {
        height: 15,
        width: 15,
        backgroundColor: '#E52139',
        '&:focus, &:hover, &$active': {
          boxShadow: 'inherit',
        },
      },
    },
  },
});

export default theme;
