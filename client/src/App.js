import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';

import { CssBaseline } from '@material-ui/core';
import { StylesProvider, ThemeProvider } from '@material-ui/core/styles';
import theme from 'styles/theme';
import Box from '@material-ui/core/Box';

import FiltersContextProvider from 'providers/FiltersContext';
import GuardedRoute from 'providers/GuardedRoute';

import Navigation from 'components/Navigation/Navigation';
import ServerError from 'components/ServerError/ServerError';
import Home from 'pages/Home/Home';
import PlaylistsList from 'pages/PlaylistsList/PlaylistsList';
import CreatePlaylist from 'pages/CreatePlaylist/CreatePlaylistStepper';
import NoMatch from 'pages/NoMatch/NoMatch';
import Login from 'pages/Login/Login';
import Register from 'pages/Register/SignUp';
import UserProfile from 'pages/UserProfile/UserProfile';
import AdminUsersList from 'pages/Admin/Users/AdminUsersList';
import AdminPlaylistsList from 'pages/Admin/Playlists/AdminPlaylistsList';
import Playlist from 'pages/Playlist/Playlist';

const App = () => (
  <ThemeProvider theme={theme}>
    <StylesProvider injectFirst>
      <CssBaseline />
      <Router>
        <Navigation />
        <Box component='main'>
          <FiltersContextProvider>
            <Switch>
              <Redirect path="/" exact to="/home" />
              <Route exact path="/home" component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/playlists/:id" component={Playlist} />
              <Route exact path="/playlists" component={PlaylistsList} />

              <GuardedRoute exact path="/create-playlist" component={CreatePlaylist} />
              <GuardedRoute exact admin path="/admin/users" component={AdminUsersList} />
              <GuardedRoute exact admin path="/admin/playlists" component={AdminPlaylistsList} />
              <GuardedRoute exact path="/profile/:username" component={UserProfile} />

              <Route path="/500" component={ServerError} />
              <Route path="*" component={NoMatch} />
            </Switch>
          </FiltersContextProvider>
        </Box>
      </Router>
    </StylesProvider>
  </ThemeProvider>
);

export default App;
