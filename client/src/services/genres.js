import axiosData, { ENDPOINTS } from 'services/serviceBase';

const getAllGenres = () => axiosData.request({
  method: 'get',
  url: ENDPOINTS.GENRES,
});

export default { getAllGenres };
