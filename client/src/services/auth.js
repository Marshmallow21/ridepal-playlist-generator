import axiosData, { ENDPOINTS } from 'services/serviceBase';

const login = (username, password) => axiosData.request({
  method: 'post',
  url: ENDPOINTS.LOGIN,
  data: { username, password },
});

const logout = () => axiosData.request({
  method: 'post',
  url: ENDPOINTS.LOGOUT,
});

export default { login, logout };
