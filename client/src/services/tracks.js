import axiosData, { ENDPOINTS } from 'services/serviceBase';
import querystring from 'querystring';

const getTracksByPlaylistId = (id, params) => axiosData.request({
  method: 'get',
  url: params
    ? `${ENDPOINTS.TRACKS}/${id}?${querystring.stringify(params)}`
    : `${ENDPOINTS.TRACKS}/${id}`,
});

export default {
  getTracksByPlaylistId
};
