import axiosData, { ENDPOINTS } from 'services/serviceBase';
import querystring from 'querystring';

const getAllPlaylists = (params) => axiosData.request({
  method: 'get',
  url: params
    ? `${ENDPOINTS.PLAYLISTS}?${querystring.stringify(params)}`
    : ENDPOINTS.PLAYLISTS,
});

const getPlaylist = (id) => axiosData.request({
  method: 'get',
  url: `${ENDPOINTS.PLAYLISTS}/${id}`,
});

const createPlaylist = (formData) => axiosData.request({
  method: 'post',
  url: ENDPOINTS.PLAYLISTS,
  data: formData,
});

const deletePlaylist = (id) => axiosData.request({
  method: 'delete',
  url: `${ENDPOINTS.PLAYLISTS}/${id}`,
});

const updatePlaylistDetails = (id, formData) => axiosData.request({
  method: 'put',
  url: `${ENDPOINTS.PLAYLISTS}/${id}`,
  data: formData,
});

export default {
  getAllPlaylists, getPlaylist, createPlaylist, deletePlaylist, updatePlaylistDetails
};
