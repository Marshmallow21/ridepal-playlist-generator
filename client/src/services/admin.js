import axiosData, { ENDPOINTS } from 'services/serviceBase';
import querystring from 'querystring';

const getAllUsers = (params) => axiosData.request({
  method: 'get',
  url: params
    ? `${ENDPOINTS.ADMIN}/users?${querystring.stringify(params)}`
    : `${ENDPOINTS.ADMIN}/users`,
});

const deleteUser = (id) => axiosData.request({
  method: 'delete',
  url: `${ENDPOINTS.ADMIN}/users/${id}`,
});

const updateUser = (id, body) => axiosData.request({
  method: 'put',
  url: `${ENDPOINTS.ADMIN}/users/${id}`,
  data: body,
});

export default {
  getAllUsers,
  deleteUser,
  updateUser
};
