# RidePal Playlist Generator

### 1. Description

Fullstack single-page application, which enables users to generate unique playlists for specific travel duration periods, based on their music preferences.

In order to generate a playlist users provide start and end point of their trip and the app calculates the travel duration. Then users can adjust their genre mix preferences by percentages, as well as choose whether to have only top tracks and whether to have multiple or only one song per artist.

Users can also browse playlists created by other users. The app has an administration part for managing users and playlists.

This is a student project, part of the final assignment of the Telerik Alpha JavaScript track.

### 2. Links

 - [Client](https://ridepal-client.herokuapp.com/)
 - [Server](https://ridepal-playlist-generator.herokuapp.com)
 - [Interactive Swagger API documentation](https://ridepal-playlist-generator.herokuapp.com/api-docs/)

** We are using a free service to host the app, so some malfunctions or downtime might be experienced. Thus we also recommend setting-up and testing the app locally (you can follow the instructions bellow).

### 3. Creators

- Tsvetelina Kyoseva - @Marshmallow21
- Yoanna Karamfilova - @YoKaramfilova

### 4. Core Back-end Technologies
 - Node.js
 - Express
 - MariaDB
  
### 5. Core Front-end Technologies
 - React
 - Material-UI
 - Axios

### 6. External Services
 - [Deezer API](https://developers.deezer.com/) - An audio streaming service, which is the source of genres, artists, albums and tracks needed for the app.
 - [Bing Maps API](https://docs.microsoft.com/en-us/bingmaps/#pivot=main&panel=BingMapsAPI) - A geolocation service, which helps us calculate the travel duration.
 - [Unsplash API](https://unsplash.com/developers) - The largest open collection of high-quality photos, which allows us to generate random playlist covers.

### 7. Features
#### Public

 - Home page with top 10 playlists, list of all genres and artists
![Home 1](/screenshots/home.png)
![Home 2](/screenshots/home-2.png)
 - Register
![Register](/screenshots/register.png)
 - Login
![Login](/screenshots/login.png)
 - All playlists page 
![Playlists](/screenshots/playlists.png)
 - Sort/filter/search functionality for playlists
![Playlists filters](/screenshots/playlists-filter.png)
 - Single playlist page with playlist details and tracks
![Playlist](/screenshots/playlist.png)
 - Filter playlist tracks by genre and artist
![Playlist filters](/screenshots/playlist-filter.png)
 - Play track previews
![Player](/screenshots/player.png)

#### Private

 - Create playlist
![Set trip start and end points](/screenshots/cp-start-end-points.png)
![Confirm duration](/screenshots/cp-duration.png)
![Choose genres](/screenshots/cp-genres.png)
![Adjust preferences](/screenshots/cp-preferences.png)
![Personalize](/screenshots/cp-personalization.png)
![Confirmation](/screenshots/cp-confirmation.png)
![Preview](/screenshots/cp-preview.png)
 - Edit own playlist
![Edit playlist](/screenshots/edit-playlist.png)
 - Delete own playlist
![Delete playlist](/screenshots/delete-playlist.png)
 - View own profile and other users' profiles
![User profile](/screenshots/user-profile.png)
 - Edit user profile details and password
![Edit user details](/screenshots/edit-user-details.png)
![Edit user password](/screenshots/change-password.png)
#### Admin

 - All playlists page
![Playlist details admin](/screenshots/playlists-admin.PNG)
 - All playlists page with sort/filter/search functionality
![Playlist details admin](/screenshots/playlists-search-admin.PNG)
 - Edit playlist
![Edit playlist details admin](/screenshots/edit-playlist-admin.PNG)
 - Delete playlist
![Delete playlist admin](/screenshots/delete-playlist-admin.PNG)
 - All users page with sort/filter functionality
![Edit user details admin](/screenshots/users-admin.PNG)
 - Edit user
![Edit user details admin](/screenshots/edit-user-details-admin.PNG)
 - Delete user
![Edit user details admin](/screenshots/delete-user-admin.PNG)
### 8. Back-end Setup

Prerequisites
 - [Node.js](https://nodejs.org/en/) installation
 - [MariaDB](https://mariadb.org/download/) installation

1. Go inside the `server` folder.
2. Run `npm install` or `npm i` to restore all dependencies.
3. You can setup a local database in two ways:
 - Import the database schema from the **database-schema** file in the **data-export** folder. After that run `npm seed` to fetch data from Deezer and add initial admin users.
 - Or directly import the **data-dump** file in the **data-export** folder, which contains prefetched Deezer data as well as some mock data for users and playlists.
4. The project is currently linked to a remote database. To connect to a local database update the .env file in the server root folder with the following configuration:
   ```js
   PORT=5555
   HOST=localhost
   DB_PORT=3306
   USER=root // Replace your MariaDB database username
   PASSWORD=1234 // Replace your MariaDB database password 
   DATABASE=ridepal
   ```
5. After that, there are a few scripts you can run:

- `npm start` - will run the current version of the code and will **not** reflect any changes done to the code until this command is executed again
- `npm run start:dev` - will run the code in *development* mode, meaning every change done to the code will trigger the program to restart and reflect the changes made

### 9. Front-end Setup

1. Go inside the `client` folder.
2. Run `npm install` or `npm i` to restore all dependencies.
3. The front-end currently uses a proxy to a remote server. To connect a local server, update the proxy in the `package.json` file in the client root folder with "http://localhost:5555".
4. After that you can run:
#### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### 10. Database schema
![Database schema](/screenshots/db-schema.jpg)
