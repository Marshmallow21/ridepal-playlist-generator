import express from 'express';
import asyncHandler from 'express-async-handler';

import transformBody from '../middlewares/transform-body.js';
import validateBody from '../middlewares/validate-body.js';
import fileUpload from '../middlewares/file-upload.js';
import authUser from '../middlewares/auth-user.js';
import tokenLogoutUsers from '../middlewares/token-logout-users.js';
import limitQuery from '../middlewares/limit-query.js';
import createPlaylistValidator from '../validators/create-playlist-validator.js';
import updatePlaylistDetailsValidator from '../validators/update-playlist-details-validator.js';

import playlistsData from '../data/playlists-data.js';
import tracksData from '../data/tracks-data.js';
import genresData from '../data/genres-data.js';

import serviceErrors from '../common/service-errors.js';
import tracksService from '../service/tracks-service.js';
import playlistsService from '../service/playlists-service.js';
import genresService from '../service/genres-service.js';

const playlistsRouter = express.Router();

/**
 * @swagger
 * /playlists:
 *   get:
 *     description: Get all playlists
 *     tags:
 *     - Get all playlists
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *        description: The number of items to skip before starting to collect the result set
 *        example: 0
 *      - in: query
 *        name: limit
 *        schema:
 *          type: integer
 *        description: The numbers of items to return
 *        example: 10
 *      - in: query
 *        name: sortBy
 *        schema:
 *          type: string
 *        description: Parameter to sort results by and direction of the sort
 *        example: title.desc
 *      - in: query
 *        name: title
 *        schema:
 *          type: string
 *        description: Playlist title to filter by
 *        example: alt
 *      - in: query
 *        name: genres
 *        schema:
 *          oneOf:
 *            - type: string
 *            - type: array
 *        description: Genre or list of genres to filter by
 *        example: Alternative
 *      - in: query
 *        name: playtime
 *        schema:
 *          oneOf:
 *            - type: string
 *            - type: array
 *        description: Playtime window or list of playtime windows to filter by
 *        example: 1801-3600
 *      - in: query
 *        name: topPlaylists
 *        schema:
 *          type: boolean
 *        description: Whether to include only top playlists
 *        example: true
 *      - in: query
 *        name: userId
 *        schema:
 *          type: integer
 *        description: Playlist creator id to filter by
 *        example: 1
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns an array of playlists & total playlist count without filter applied.
 *         schema:
 *           type: object
 *           properties:
 *             total:
 *               type: integer
 *             data:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                    id:
 *                      type: integer
 *                    title:
 *                      type: string
 *                    playtime:
 *                      type: integer
 *                    rank:
 *                      type: number
 *                    picture:
 *                      type: string
 *                    created_on:
 *                      type: string
 *                    user_id:
 *                      type: integer
 *                    is_deleted:
 *                      type: integer
 *                    genre_id:
 *                      type: integer
 *                    username:
 *                      type: string
 *                    genres:
 *                      type: string
 *                    total:
 *                      type: integer
 */
playlistsRouter.get('/',
  limitQuery,
  asyncHandler(async (req, res) => {
    const result = await playlistsService.getAllPlaylists(playlistsData)(req.query);

    res
      .status(200)
      .json(result);
  }));

/**
 * @swagger
 * /playlists/{id}:
 *   get:
 *     description: Get playlist by id
 *     tags:
 *     - Get playlist by id
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the playlist
 *        example: 1
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns a playlist object
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             title:
 *               type: string
 *             playtime:
 *               type: integer
 *             rank:
 *               type: number
 *             picture:
 *               type: string
 *             createdOn:
 *               type: string
 *             userId:
 *               type: integer
 *             is_deleted:
 *               type: integer
 *             username:
 *               type: string
 *             genres:
 *               type: string
 *       404:
 *         description: Playlist not found.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
playlistsRouter.get('/:id',
  asyncHandler(async (req, res) => {
    const { id } = req.params;

    const result = await playlistsService.getPlaylistById(playlistsData)(+id);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `Playlist with id ${id} not found.` });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

/**
 * @swagger
 * /playlists:
 *   post:
 *     description: Create a playlist
 *     tags:
 *     - Create a playlist
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     consumes:
 *       multipart/form-data
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/CreatePlaylist'
 *           encoding:
 *             image:
 *               contentType: image/png, image/jpeg
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns a playlist object
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             title:
 *               type: string
 *             playtime:
 *               type: integer
 *             rank:
 *               type: number
 *             picture:
 *               type: string
 *             createdOn:
 *               type: string
 *             userId:
 *               type: integer
 *             is_deleted:
 *               type: integer
 *             username:
 *               type: string
 *             genres:
 *               type: string
 *       404:
 *         description: No tracks found for some of the selected genres and filters.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       400:
 *         description: Not enough tracks for the applied filters or invalid image.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       409:
 *         description: Playlist with this title already exist.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
playlistsRouter.post('/',
  authUser,
  asyncHandler(tokenLogoutUsers),
  fileUpload,
  transformBody(createPlaylistValidator),
  validateBody('playlist', createPlaylistValidator),
  asyncHandler(async (req, res) => {
    const {
      duration, genres, repeatArtists, topTracks, title, imageURL,
    } = req.body;

    const tracks = await tracksService
      .generatePlaylistTracks(tracksData)(duration, JSON.parse(genres), topTracks, repeatArtists);

    if (tracks.error === serviceErrors.RECORD_NOT_FOUND) {
      return res
        .status(404)
        .json({ message: 'No tracks found for some of the selected genres and filters.' });
    }

    if (tracks.error === serviceErrors.INVALID_REQUEST) {
      return res
        .status(400)
        .json({ message: 'Not enough tracks for the applied filters.' });
    }

    const playlist = await playlistsService
      .createPlaylist(playlistsData)(title, req.file, imageURL, req.user.id);

    if (playlist.error === serviceErrors.DUPLICATE_RECORD) {
      return res
        .status(409)
        .json({ message: `Playlist with title ${title} already exists.` });
    }

    if (playlist.error === serviceErrors.INVALID_REQUEST) {
      return res
        .status(400)
        .json({ message: 'Playlist image should be in .png, .jpg or .jpeg format.' });
    }

    await Promise.all(tracks.data.map(async (track) => {
      await tracksService
        .addTrackToPlaylist(tracksData)(playlist.data.id, track.id, track.deezer_id);
    }));

    await playlistsService
      .updatePlaylistPlaytime(playlistsData)(tracks.totalDuration, playlist.data.id);

    await playlistsService
      .updatePlaylistRank(playlistsData)(tracks.avgRank, playlist.data.id);

    await Promise.all(Object.keys(JSON.parse(genres)).map(async (genre) => {
      await genresService
        .addPlaylistToGenre(genresData)(genre, playlist.data.id);
    }));

    res
      .status(200)
      .json(await playlistsData.getBy('id', playlist.data.id));
  }));

/**
 * @swagger
 * /playlists/{id}:
 *   delete:
 *     description: Delete playlist by id
 *     tags:
 *     - Delete playlist by id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the playlist
 *        example: 1
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns a playlist object
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             title:
 *               type: string
 *             playtime:
 *               type: integer
 *             rank:
 *               type: number
 *             picture:
 *               type: string
 *             createdOn:
 *               type: string
 *             userId:
 *               type: integer
 *             is_deleted:
 *               type: integer
 *             username:
 *               type: string
 *             genres:
 *               type: string
 *       404:
 *         description: Playlist not found.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
playlistsRouter.delete('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  asyncHandler(async (req, res) => {
    const result = await playlistsService.removePlaylist(playlistsData)(+req.params.id);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `Playlist with id ${+req.params.id} not found.` });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

/**
 * @swagger
 * /playlists/{id}:
 *   put:
 *     description: Update playlist details by id
 *     tags:
 *     - Update playlist details by id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the playlist
 *        example: 1
 *     consumes:
 *       multipart/form-data
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/UpdatePlaylistDetails'
 *           encoding:
 *             image:
 *               contentType: image/png, image/jpeg
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns a playlist object
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: integer
 *             title:
 *               type: string
 *             playtime:
 *               type: integer
 *             rank:
 *               type: number
 *             picture:
 *               type: string
 *             createdOn:
 *               type: string
 *             userId:
 *               type: integer
 *             is_deleted:
 *               type: integer
 *             username:
 *               type: string
 *             genres:
 *               type: string
 *       400:
 *         description: Invalid image format.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       403:
 *         description: Not allowed to update this playlist.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       409:
 *         description: Playlist with this title already exist.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
playlistsRouter.put('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  fileUpload,
  transformBody(updatePlaylistDetailsValidator),
  validateBody('playlist', updatePlaylistDetailsValidator),
  asyncHandler(async (req, res) => {
    const result = await playlistsService
      .updatePlaylistDetails(playlistsData)(+req.params.id, req.body, req.file);

    if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res
        .status(403)
        .json({ message: 'Not allowed to update this playlist.' });
    } else if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'Playlist with this title already exist.' });
    } else if (result.error === serviceErrors.INVALID_REQUEST) {
      res
        .status(400)
        .json({ message: 'Please add an image in .png, .jpg and .jpeg format to upload.' });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

export default playlistsRouter;
