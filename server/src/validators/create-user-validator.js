export default {
  username: (value) => typeof value === 'string'
    && value.match(/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g),
  email: (value) => typeof value === 'string'
    && value.length > 6
    && value.match(/^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-z]+$/g),
  password: (value) => typeof value === 'string'
    && value.match(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g),
};
