export default {
  duration: (value) => typeof value === 'string'
    && +value >= 5 * 60
    && +value <= 24 * 60 * 60,
  genres: (value) => typeof value === 'string'
    && Object.values(JSON.parse(value)).length !== 0
    && Object.values(JSON.parse(value)).reduce((acc, curr) => acc + curr) === 100,
  repeatArtists: (value) => value === undefined
    || value === 'on',
  topTracks: (value) => value === undefined
    || value === 'on',
  title: (value) => typeof value === 'string'
    && value.length >= 1
    && value.length <= 255,
  imageURL: (value) => value === undefined
    || (typeof value === 'string' && value.length <= 255),
};
