export default {
  newPassword: (value) => typeof value === 'string'
    && value.match(/^(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g),
  oldPassword: (value) => typeof value === 'string'
    && value.match(/^(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g),
};
