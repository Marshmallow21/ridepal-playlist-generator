export default {
  title: (value) => value === undefined
    || (typeof value === 'string'
      && value.length >= 1
      && value.length <= 255),
  imageURL: (value) => value === undefined
    || (value === undefined
      || typeof value === 'string'),
};
