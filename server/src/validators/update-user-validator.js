export default {
  username: (value) => value === undefined
    || (typeof value === 'string'
      && value.match(/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[A-z\d._]+(?<![_.])$/g)),
  email: (value) => value === undefined
    || (typeof value === 'string'
      && value.length > 6
      && value.match(/^(?![_.-])(?!.*[_.-]{2})[A-z\d_.-]+@[A-z\d-]+\.[a-z]+$/g)),
  password: (value) => value === undefined
    || (typeof value === 'string'
      && value.match(/^(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g)),
  role_id: (value) => value === undefined
    || (typeof +value === 'number'),
};
