import pool from './pool.js';

const getAll = async () => {
  const sql = `
    SELECT * FROM genres
    `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
    SELECT *
    FROM genres
    WHERE ${column} = ?
    `;

  return (await pool.query(sql, [value]))[0];
};

const addPlaylistToGenre = async (genreId, genreDeezerId, playlistId) => {
  const sql = `
    INSERT INTO genres_has_playlists (genre_id, genre_deezer_id, playlist_id)
    VALUES (?, ?, ?)`;

  return await pool.query(sql, [genreId, genreDeezerId, playlistId]);
};

export default {
  getAll,
  getBy,
  addPlaylistToGenre,
};
