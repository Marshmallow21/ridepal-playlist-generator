import mariadb from 'mariadb';
import config from '../config.js';

const { env } = config;

const pool = mariadb.createPool({
  host: env.HOST,
  port: env.DB_PORT,
  user: env.USER,
  password: env.PASSWORD,
  database: env.DATABASE,
  connectionLimit: 5,
});

export default pool;
