import fetch from 'node-fetch';
import { RateLimit } from 'async-sema';
import bcrypt from 'bcryptjs';
import db from './pool.js';

const limit = RateLimit(10);

const fetchFromDeezer = (url) => {
  const response = fetch(url)
    .then((res) => res.json())
    .then((res) => res.data)
    .catch((err) => console.log(err));
  return response;
};

const selectedGenres = ['Rock', 'Alternative', 'Metal'];
const selectedGenresMap = new Map();
const roles = ['admin', 'user'];
const users = [
  {
    username: 'Tsveti_Kyoseva',
    email: 'tsveti@gmail.com',
    password: 'Password123*',
  },
  {
    username: 'Yo_Karamfilova',
    email: 'ykaramfilova@gmail.com',
    password: 'Password123*',
  },
];

(async () => {
  const genresDB = await db.query('SELECT * FROM genres');

  if (!genresDB || genresDB.length === 0) {
    console.log('Creating genres...');

    await limit();
    const genresDeezer = (await fetchFromDeezer('https://api.deezer.com/genre'))
      .filter(genre => selectedGenres.includes(genre.name));

    await Promise.all(genresDeezer.map(({ id, name, picture }) => db.query(`
        INSERT INTO genres (deezer_id, name, picture)
        VALUES (?, ?, ?)
        `, [id, name, picture])));
  }

  const artistsDB = await db.query('SELECT * FROM artists');

  if (!artistsDB || artistsDB.length === 0) {
    console.log('Creating artists...');

    const genres = await db.query('SELECT * FROM genres');

    await Promise.all(genres.map(async (genre) => {
      await limit();
      const genreArtists = await fetchFromDeezer(`https://api.deezer.com/genre/${genre.deezer_id}/artists`);

      await Promise.all(genreArtists.map(({ id, name, picture, tracklist }) => db.query(`
        INSERT INTO artists (deezer_id, name, picture, tracklist, genre_id, genre_deezer_id)
        VALUES (?, ?, ?, ?, ?, ?)
        ON DUPLICATE KEY UPDATE
          name = ?
        `, [id, name, picture, tracklist, genre.id, genre.deezer_id, name])));
    }));
  }

  const albumsDB = await db.query('SELECT * FROM albums');

  if (!albumsDB || albumsDB.length === 0) {
    console.log('Creating albums...');

    const genres = await db.query('SELECT * FROM genres');

    genres.forEach(genre => selectedGenresMap.set(genre.deezer_id, genre.id));

    const artists = await db.query('SELECT * FROM artists');

    await Promise.all(artists.map(async (artist) => {
      await limit();
      const artistAlbums = await fetchFromDeezer(`https://api.deezer.com/artist/${artist.deezer_id}/albums?limit=5`);

      await Promise.all(artistAlbums.map(({ id, title, cover, tracklist, genre_id }) => {
        if (selectedGenresMap.has(genre_id)) {
          db.query(`
        INSERT INTO albums (deezer_id, title, cover, tracklist, artist_id, artist_deezer_id, genre_id, genre_deezer_id)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?)
        ON DUPLICATE KEY UPDATE
          title = ?
        `, [id, title, cover, tracklist, artist.id, artist.deezer_id, selectedGenresMap.get(genre_id), genre_id, title]);
        }
      }));
    }));
  }

  const tracksDB = await db.query('SELECT * FROM tracks');

  if (!tracksDB || tracksDB.length === 0) {
    console.log('Creating tracks...');

    const albums = await db.query('SELECT * FROM albums');

    await Promise.all(albums.map(async (album) => {
      await limit();
      const albumTracks = await fetchFromDeezer(`https://api.deezer.com/album/${album.deezer_id}/tracks`);

      await Promise.all(albumTracks.map(({ id, title, link, duration, rank, preview }) => db.query(`
        INSERT INTO tracks (deezer_id, title, link, duration, rank, preview, album_id, album_deezer_id, artist_id, artist_deezer_id, genre_id, genre_deezer_id)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ON DUPLICATE KEY UPDATE
          title = ?
        `, [id, title, link, duration, rank, preview, album.id, album.deezer_id, album.artist_id, album.artist_deezer_id, album.genre_id, album.genre_deezer_id, title])));
    }));
  }

  const rolesDB = await db.query('SELECT * FROM roles');

  if (!rolesDB || rolesDB.length === 0) {
    console.log('Creating roles...');

    await Promise.all(roles.map((role) => db.query(`
        INSERT INTO roles (role)
        VALUES (?)
        `, [role])));
  }

  const usersDB = await db.query('SELECT * FROM users');

  if (!usersDB || usersDB.length === 0) {
    console.log('Creating admins...');

    await Promise.all(users.map(async ({ username, email, password }) => db.query(`
        INSERT INTO users (username, email, password, role_id)
        VALUES (?, ?, ?, ?)
        `, [username, email, await bcrypt.hash(password, 10), roles.indexOf('admin') + 1])));
  }

  db.end();
})()
  .catch(console.log);
