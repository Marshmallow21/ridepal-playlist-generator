export default async (req, res, next) => {
  const { limit } = req.query;

  if (!limit || limit > 500) {
    req.query.limit = 500;
  }

  await next();
};
