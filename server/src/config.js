import dotenv from 'dotenv';

const env = dotenv.config().parsed;

const auth = {
  PRIVATE_KEY: 'bwBdYr6UG2nhguIN9Gs2',
  TOKEN_LIFETIME: 240 * 60,
};

const swaggerOptions = {
  swaggerDefinition: {
    openapi: '3.0.3',
    info: {
      title: 'RidePal – Playlist Generator',
      version: '1.0.0',
    },
    servers: [
      {
        url: 'https://ridepal-playlist-generator.herokuapp.com/v1',
        description: 'Development server',
      },
    ],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
      schemas: {
        Login: {
          type: 'object',
          properties: {
            username: {
              type: 'string',
              required: true,
              example: 'Yo_Karamfilova',
            },
            password: {
              type: 'string',
              required: true,
              example: 'Password123*',
            },
          },
        },
        Register: {
          type: 'object',
          properties: {
            username: {
              type: 'string',
              required: true,
              example: 'Todor_Petrov',
            },
            email: {
              type: 'string',
              required: true,
              example: 'todor_petrov@test.com',
            },
            password: {
              type: 'string',
              required: true,
              example: 'Password123*',
            },
          },
        },
        CreatePlaylist: {
          type: 'object',
          properties: {
            title: {
              type: 'string',
              required: true,
              example: 'Road trip Playlist',
            },
            duration: {
              type: 'string',
              required: true,
              example: '3600',
            },
            genres: {
              type: 'string',
              required: true,
              example: '{"2":20,"3":80}',
            },
            topTracks: {
              type: 'string',
              required: false,
              example: 'on',
            },
            repeatArtists: {
              type: 'string',
              required: false,
              example: 'on',
            },
            imageURL: {
              type: 'string',
              required: false,
              example: 'https://images.unsplash.com/photo-1579984505074-6f6701b369f9',
            },
            image: {
              type: 'string',
              required: false,
              format: 'binary',
              contentType: 'image/png, image/jpeg',
            },
          },
        },
        UpdateUserDetails: {
          type: 'object',
          properties: {
            username: {
              type: 'string',
              required: false,
              example: 'TodorPetrov',
            },
            email: {
              type: 'string',
              required: false,
              example: 'todor_petrov@gmail.com',
            },
            image: {
              type: 'string',
              required: false,
              format: 'binary',
              contentType: 'image/png, image/jpeg',
            },
          },
        },
        UpdatePlaylistDetails: {
          type: 'object',
          properties: {
            title: {
              type: 'string',
              required: false,
              example: 'Road trip Playlist',
            },
            imageURL: {
              type: 'string',
              required: false,
              example: 'https://images.unsplash.com/photo-1579984505074-6f6701b369f9',
            },
            image: {
              type: 'string',
              required: false,
              format: 'binary',
              contentType: 'image/png, image/jpeg',
            },
          },
        },
        UpdateUserPassword: {
          type: 'object',
          properties: {
            newPassword: {
              type: 'string',
              required: true,
              example: 'NewPassword123*',
            },
            oldPassword: {
              type: 'string',
              required: true,
              example: 'Password123*',
            },
          },
        },
        UpdateUserByAdmin: {
          type: 'object',
          properties: {
            username: {
              type: 'string',
              required: false,
              example: 'Todor_Petrov',
            },
            email: {
              type: 'string',
              required: false,
              example: 'todor_petrov@test.com',
            },
            password: {
              type: 'string',
              required: false,
              example: 'Password123*',
            },
            role_id: {
              type: 'string',
              required: false,
              example: 1,
            },
          },
        },
      },
    },
  },
  apis: ['src/routers/*.js'],
};

export default {
  env,
  auth,
  swaggerOptions,
};
