const getAllArtists = artistsData => async () => await artistsData.getAll();

export default {
  getAllArtists,
};
