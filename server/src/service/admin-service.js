import bcrypt from 'bcryptjs';
import serviceErrors from '../common/service-errors.js';

const updateUser = (usersData) => async (id, data) => {
  const { username, email, password, role_id } = data;

  if (username) {
    const existingName = await usersData.getBy('username', username);

    if (existingName) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        data: null,
      };
    }

    await usersData.update('username', username, id);
  }

  if (email) {
    const existingEmail = await usersData.getBy('email', email);

    if (existingEmail) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        data: null,
      };
    }

    await usersData.update('email', email, id);
  }

  if (password) {
    await usersData.update('password', await bcrypt.hash(password, 10), id);
  }

  if (role_id) {
    await usersData.update('role_id', role_id, id);
  }

  return {
    error: null,
    data: await usersData.getBy('id', id),
  };
};

export default {
  updateUser,

};
