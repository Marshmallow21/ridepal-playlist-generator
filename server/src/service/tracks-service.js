import serviceErrors from '../common/service-errors.js';
import constants from '../common/constants.js';

const getAllByPlaylist = tracksData => (
  async (id, query) => await tracksData.getAllByPlaylist(id, query)
);

const generatePlaylistTracks = tracksData => (
  async (duration, genres, topTracks, repeatArtists) => {
    const playlistTracks = [];
    const artists = new Set();

    for (let i = 0; i < Object.entries(genres).length; i++) {
      const [genreId, playtimePercent] = Object.entries(genres)[i];

      const genreTracks = await tracksData.getRandomizedTracksByGenre(genreId, topTracks);

      if (genreTracks.length === 0) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: null,
        };
      }

      const genrePlaytimeOffBy = constants.PLAYTIME_OFF_BY * (playtimePercent / 100);
      const minGenrePlaytime = duration * (playtimePercent / 100) - genrePlaytimeOffBy;
      const maxGenrePlaytime = duration * (playtimePercent / 100) + genrePlaytimeOffBy;
      let genrePlaytime = 0;

      while (genrePlaytime < minGenrePlaytime) {
        if (genreTracks.length === 0) {
          return {
            error: serviceErrors.INVALID_REQUEST,
            data: null,
          };
        }
        const currentTrack = genreTracks.pop();

        if (currentTrack.duration <= (maxGenrePlaytime - genrePlaytime)) {
          if (repeatArtists || !artists.has(currentTrack.artist_id)) {
            genrePlaytime += currentTrack.duration;
            artists.add(currentTrack.artist_id);
            playlistTracks.push(currentTrack);
          }
        }
      }
    }

    return {
      error: null,
      data: playlistTracks,
      totalDuration: playlistTracks.reduce((acc, curr) => acc + curr.duration, 0),
      avgRank: playlistTracks.reduce((acc, curr) => acc + curr.rank, 0) / playlistTracks.length,
    };
  });

const addTrackToPlaylist = tracksData => (
  async (playlistId, trackId, trackDeezerId) => ({
    error: null,
    data: await tracksData.addTrackToPlaylist(playlistId, trackId, trackDeezerId),
  })
);

export default {
  getAllByPlaylist,
  generatePlaylistTracks,
  addTrackToPlaylist,
};
