const getAllGenres = genresData => async () => await genresData.getAll();

const addPlaylistToGenre = genresData => async (genreId, playlistId) => {
  const genre = await genresData.getBy('id', genreId);

  return {
    error: null,
    data: await genresData.addPlaylistToGenre(genreId, genre.deezer_id, playlistId),
  };
};

export default {
  getAllGenres,
  addPlaylistToGenre,
};
